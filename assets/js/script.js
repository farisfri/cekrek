$(document).ready(function () {
	$('#dataPekerja').DataTable();
	$('.dataTables_length').addClass('bs-select');
	$('[data-toggle="tooltip"]').tooltip();
	$('.datepicker').datepicker({ 
		format: 'yyyy-mm-dd', 
	});
	$('.custom-select-role').select2();

	$('#dtHorizontalExample').DataTable({
		"scrollX": true,
	});
	
	$('.dataTables_length').addClass('bs-select');

});
