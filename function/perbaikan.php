<?php

function tampilMtc(){
	$query = "SELECT * FROM tipe_perbaikan";
	return result($query);
}

function tampilPerbaikan(){
	$query = "SELECT
			mtc_id, mtc_mesinId, mtc_perintahId, mtc_startmtc, mtc_endmtc, mtc_keterangan,
			mtc_catatanMec, mtc_tipemtc, mtc_statusMesin, mtc_crtuser, mtc_pelaksana,
			mtc_resultStat,
			D.nama_depan AS nd_exe1, D.nama_belakang AS nb_exe1,
			G.nama_depan AS nd_exe2, G.nama_belakang AS nb_exe2,
			J.nama_depan AS nd_exe3, J.nama_belakang AS nb_exe3,
			K.mesin_id AS msn_msn_id,
			K.mesin_nama AS prt_nama_mesin, K.mesin_jenis AS prt_jenis_mesin,
			K.mesin_tipe AS prt_tipe_mesin, K.mesin_deskripsi AS prt_deskripsi_mesin,
			K.mesin_tglbeli AS prt_tglbeli_mesin,
			K.mesin_code AS prt_kode_mesin, K.mesin_blok AS prt_blok_mesin,
			K.mesin_line AS prt_line_mesin, K.mesin_nomor AS prt_nomor_mesin,
			L.prio_keterangan AS prt_ket_priori, N.nama_depan AS prt_namdep_hmd,
			N.nama_belakang AS prt_nambel_hmd, O.perintah_judul AS prt_judul,
			O.perintah_crtdate AS prt_crtdate
		FROM
			perbaikan AS A
			INNER JOIN pelaksana AS B ON(A.mtc_pelaksana = B.exe_id)
			INNER JOIN pekerja_data AS C ON(B.exe_pelaksana1 = C.pekerja_id)
			INNER JOIN pekerja_bio AS D ON(C.krywn_bio = D.bio_id)

			INNER JOIN pelaksana AS E ON(A.mtc_pelaksana = E.exe_id)
			INNER JOIN pekerja_data AS F ON(E.exe_pelaksana2 = F.pekerja_id)
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)

			INNER JOIN pelaksana AS H ON(A.mtc_pelaksana = H.exe_id)
			INNER JOIN pekerja_data AS I ON(H.exe_pelaksana3 = I.pekerja_id)
			INNER JOIN pekerja_bio AS J ON(I.krywn_bio = J.bio_id)

			INNER JOIN mesin_data AS K ON(A.mtc_mesinId = K.mesin_id)
			INNER JOIN prioritas AS L ON(A.mtc_prioritas = L.prio_id)
			INNER JOIN pekerja_data AS M ON(A.mtc_crtuser = M.pekerja_id)
			INNER JOIN pekerja_bio AS N ON(M.pekerja_id = N.bio_id)
			INNER JOIN perintah AS O ON(A.mtc_perintahId = O.perintah_id)";
	return result($query);
}


function tampilkan_per_perbaikan($IdPerbaikan){
	$query = "SELECT
			mtc_id, mtc_mesinId, mtc_perintahId, mtc_startmtc, mtc_endmtc, mtc_keterangan,
			mtc_catatanMec, mtc_tipemtc, mtc_statusMesin, mtc_crtuser, mtc_pelaksana,
			mtc_resultStat,
			D.nama_depan AS nd_exe1, D.nama_belakang AS nb_exe1,
			G.nama_depan AS nd_exe2, G.nama_belakang AS nb_exe2,
			J.nama_depan AS nd_exe3, J.nama_belakang AS nb_exe3,
			K.mesin_id AS msn_Id_mesin,K.mesin_nama AS msn_nama_mesin, K.mesin_jenis AS msn_jenis_mesin,
			K.mesin_tipe AS msn_tipe_mesin, K.mesin_deskripsi AS msn_deskripsi_mesin,
			K.mesin_tglbeli AS msn_tglbeli_mesin,
			K.mesin_code AS msn_kode_mesin, K.mesin_blok AS msn_blok_mesin,
			K.mesin_line AS msn_line_mesin, K.mesin_nomor AS msn_nomor_mesin,
			L.prio_keterangan AS pri_ket_priori, N.nama_depan AS prt_namdep_hmd,
			N.nama_belakang AS prt_nambel_hmd, O.perintah_judul AS prt_judul,
			O.perintah_crtdate AS prt_crtdate, O.perintah_keterangan AS prt_keterangan,
			O.perintah_catatan AS prt_catatan
		FROM
			perbaikan AS A
			INNER JOIN pelaksana AS B ON(A.mtc_pelaksana = B.exe_id)
			INNER JOIN pekerja_data AS C ON(B.exe_pelaksana1 = C.pekerja_id)
			INNER JOIN pekerja_bio AS D ON(C.krywn_bio = D.bio_id)

			INNER JOIN pelaksana AS E ON(A.mtc_pelaksana = E.exe_id)
			INNER JOIN pekerja_data AS F ON(E.exe_pelaksana2 = F.pekerja_id)
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)

			INNER JOIN pelaksana AS H ON(A.mtc_pelaksana = H.exe_id)
			INNER JOIN pekerja_data AS I ON(H.exe_pelaksana3 = I.pekerja_id)
			INNER JOIN pekerja_bio AS J ON(I.krywn_bio = J.bio_id)

			INNER JOIN mesin_data AS K ON(A.mtc_mesinId = K.mesin_id)
			INNER JOIN prioritas AS L ON(A.mtc_prioritas = L.prio_id)
			INNER JOIN pekerja_data AS M ON(A.mtc_crtuser = M.pekerja_id)
			INNER JOIN pekerja_bio AS N ON(M.pekerja_id = N.bio_id)
			INNER JOIN perintah AS O ON(A.mtc_perintahId = O.perintah_id) WHERE mtc_id=$IdPerbaikan";
	return result($query);
}

function notif_perbaikan($mesin_idDetail){
	$query = "SELECT
			mtc_id, mtc_mesinId, mtc_perintahId, mtc_startmtc, mtc_endmtc, mtc_keterangan,
			mtc_catatanMec, mtc_tipemtc, mtc_statusMesin, mtc_crtuser, mtc_pelaksana,
			mtc_resultStat,
			D.nama_depan AS nd_exe1, D.nama_belakang AS nb_exe1,
			G.nama_depan AS nd_exe2, G.nama_belakang AS nb_exe2,
			J.nama_depan AS nd_exe3, J.nama_belakang AS nb_exe3,
			K.mesin_nama AS msn_nama_mesin, K.mesin_jenis AS msn_jenis_mesin,
			K.mesin_tipe AS msn_tipe_mesin, K.mesin_deskripsi AS msn_deskripsi_mesin,
			K.mesin_tglbeli AS msn_tglbeli_mesin,
			K.mesin_code AS msn_kode_mesin, K.mesin_blok AS msn_blok_mesin,
			K.mesin_line AS msn_line_mesin, K.mesin_nomor AS msn_nomor_mesin,
			L.prio_keterangan AS pri_ket_priori, N.nama_depan AS prt_namdep_hmd,
			N.nama_belakang AS prt_nambel_hmd, O.perintah_judul AS prt_judul,
			O.perintah_crtdate AS prt_crtdate
		FROM
			perbaikan AS A
			INNER JOIN pelaksana AS B ON(A.mtc_pelaksana = B.exe_id)
			INNER JOIN pekerja_data AS C ON(B.exe_pelaksana1 = C.pekerja_id)
			INNER JOIN pekerja_bio AS D ON(C.krywn_bio = D.bio_id)

			INNER JOIN pelaksana AS E ON(A.mtc_pelaksana = E.exe_id)
			INNER JOIN pekerja_data AS F ON(E.exe_pelaksana2 = F.pekerja_id)
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)

			INNER JOIN pelaksana AS H ON(A.mtc_pelaksana = H.exe_id)
			INNER JOIN pekerja_data AS I ON(H.exe_pelaksana3 = I.pekerja_id)
			INNER JOIN pekerja_bio AS J ON(I.krywn_bio = J.bio_id)

			INNER JOIN mesin_data AS K ON(A.mtc_mesinId = K.mesin_id)
			INNER JOIN prioritas AS L ON(A.mtc_prioritas = L.prio_id)
			INNER JOIN pekerja_data AS M ON(A.mtc_crtuser = M.pekerja_id)
			INNER JOIN pekerja_bio AS N ON(M.pekerja_id = N.bio_id)
			INNER JOIN perintah AS O ON(A.mtc_perintahId = O.perintah_id) 
			WHERE mtc_mesinId=$mesin_idDetail";
	return result($query);
}

function kendala_perbaikan($mtc_tipemtc, $mtc_keterangan, $mtc_catatanMec, $mtc_id_a){
	$mtc_keterangan = escape(htmlentities($mtc_keterangan));
	$mtc_catatanMec = escape(htmlentities($mtc_catatanMec));

	$query="UPDATE perbaikan SET mtc_tipemtc='$mtc_tipemtc', mtc_keterangan='$mtc_keterangan', mtc_catatanMec='$mtc_catatanMec', mtc_resultStat=2
	WHERE mtc_id=$mtc_id_a";

	return run($query);
}

function selesai_perbaikan($mtc_tipemtc, $mtc_keterangan, $mtc_catatanMec, $mtc_id_a){
	$mtc_keterangan = escape(htmlentities($mtc_keterangan));
	$mtc_catatanMec = escape(htmlentities($mtc_catatanMec));
	$now = date("Y-m-d");

	$query="UPDATE perbaikan SET mtc_tipemtc='$mtc_tipemtc', mtc_keterangan='$mtc_keterangan', mtc_catatanMec='$mtc_catatanMec', mtc_endmtc='$now', mtc_resultStat=3
	WHERE mtc_id=$mtc_id_a";

	return run($query);
}

function mulai_perbaikan($IdPerbaikan){
	$now = date("Y-m-d");

	$query="UPDATE perbaikan SET mtc_startmtc='$now', mtc_statusMesin=1, mtc_resultStat=1
	WHERE mtc_id=$IdPerbaikan";

	return run($query);
}

function gantiStts_Perbaikan($mtc_id_a){
	$mtcId = escape($mtc_id_a);

	$query="UPDATE perbaikan SET mtc_resultStat=4
	WHERE mtc_id=$mtc_id_a";

	return run($query);
}

function perawatan_cek($mtc_tipemtc_a){
	$tipemtc1 = escape($mtc_tipemtc_a);

	$query = "SELECT prev_tipemtc FROM perawatan WHERE prev_tipemtc='$tipemtc1'";

global $link;

	if ($result= mysqli_query($link, $query)) {
		if(mysqli_num_rows($result) == 0) return true;
		else return false;
	}
}

function rilis_perawatan($msn_Id_mesin_a, $msn_tglbeli_mesin_a, $mtc_startmtc_a, $prt_judul_a, $mtc_tipemtc_a, $mtc_pelaksana_a){
	$msn_Id_mesin = $msn_Id_mesin_a;
	$msn_tglbeli_mesin = $msn_tglbeli_mesin_a;
	$mtc_startmtc = $mtc_startmtc_a;
	$prt_judul = $prt_judul_a;
	$mtc_tipemtc = $mtc_tipemtc_a;
	$mtc_pelaksana = $mtc_pelaksana_a;

	$date1=date_create("$msn_tglbeli_mesin");
    $date2=date_create("$mtc_startmtc");
    $diff=date_diff($date1,$date2);

	$query = "INSERT INTO perawatan (prev_id, prev_mesinId, prev_startmtc, prev_endmtc, diff_startend, prev_namaperbaikan, prev_tipemtc, prev_statusMesin, prev_pelaksana, prev_resultStat, prev_priority) VALUES 
	('','$msn_Id_mesin','$msn_tglbeli_mesin','$mtc_startmtc', '".$diff->days."','$prt_judul','$mtc_tipemtc',1,'$mtc_pelaksana_a',3,0)";

	return run($query);
}

function rilis_perawatan_b($msn_Id_mesin_a, $mtc_startmtc_a, $mtc_endmtc_a, $prt_judul_a, $mtc_tipemtc_a, $mtc_pelaksana_a){
	$msn_Id_mesin = $msn_Id_mesin_a;	
	$mtc_startmtc = $mtc_startmtc_a;
	$mtc_endmtc = $mtc_endmtc_a;
	$prt_judul = $prt_judul_a;
	$mtc_tipemtc = $mtc_tipemtc_a;
	$mtc_pelaksana = $mtc_pelaksana_a;

	$datea1=date_create("$mtc_startmtc");
    $datea2=date_create("$mtc_endmtc");
    $diffa=date_diff($datea1,$datea2);

	$query = "INSERT INTO perawatan (prev_id, prev_mesinId, prev_startmtc, prev_endmtc, diff_startend, prev_namaperbaikan, prev_tipemtc, prev_statusMesin, prev_pelaksana, prev_resultStat, prev_priority) VALUES 
	('','$msn_Id_mesin','$mtc_startmtc','$mtc_endmtc', '".$diffa->days."','$prt_judul','$mtc_tipemtc',2,'$mtc_pelaksana',3,0)";
	return run($query);
}

function register_tipemtc($tmtc_jenis, $tmtc_keterangan){
	$tmtc_jenis = escape(htmlentities($tmtc_jenis));
	$tmtc_keterangan = escape(htmlentities($tmtc_keterangan));

	$query = "INSERT INTO tipe_perbaikan (tmtc_id, tmtc_jenis, tmtc_keterangan) VALUES ('','$tmtc_jenis','$tmtc_keterangan')";
	return run($query);
}
