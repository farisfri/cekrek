<?php

function tampil(){
	$query = "SELECT * FROM pekerja_data";
	return result($query);
}

function tampilUsername($usid){
    $query = "SELECT * FROM pekerja_data INNER JOIN pekerja_bio ON pekerja_data.pekerja_id=pekerja_bio.bio_id WHERE krywn_username='$usid'";   
    return result($query);
}

function tampil_biodata(){
	$query = "SELECT * FROM pekerja_bio";
	return result($query);
}

function tampil_pekerja(){
	$query = "SELECT * FROM pekerja_data INNER JOIN pekerja_bio ON pekerja_data.pekerja_id=pekerja_bio.bio_id";
	return result($query);
}

function tampilkan_per_user($krywn_id){
	$query = "SELECT * FROM pekerja_data WHERE krywn_id=$krywn_id";
	return result($query);
}

function tampilkrywn_email($krywn_email){
	$query = "SELECT id FROM pekerja_data WHERE krywn_email=$krywn_email";
	return result($query);
}


function edit_user($username, $nama, $krywn_email, $no_hp, $kit_type, $role, $id){
	$query="UPDATE pekerja_data SET username='$username', nama='$nama', krywn_email='$krywn_email', no_hp='$no_hp', kit_type='$kit_type', role='$role'
	WHERE id=$id";

	return run($query);
}

function user_edit( $nama, $no_hp, $id){
    $query="UPDATE pekerja_data SET nama='$nama', no_hp='$no_hp'
    WHERE id=$id";

    return run($query);
}

function register_kodeqr($krywn_code, $krywn_qrcode, $pekerja_id){

    $query="UPDATE pekerja_data SET krywn_code='$krywn_code', krywn_qrcode='$krywn_qrcode'
    WHERE pekerja_id=$pekerja_id";

    return run($query);
}

// function edit_userPass($pass, $id){
// 	$encryptionMethod = "AES-256-CBC";
// 	$secretHash = "1hwvyc28xmmy";
// 	$encrypted = openssl_encrypt($pass, $encryptionMethod, $secretHash);

// 	$query="UPDATE pekerja_data SET password='$encrypted'
// 	WHERE id=$id";

// 	return run($query);
// }


function hapus_user($id){
	$query="DELETE FROM pekerja_data WHERE id=$id";
	return run($query);
}

function register_biodata($nama_depan, $nama_belakang, $tgl_lahir, $no_hp){
	$nama_depan = escape(htmlentities($nama_depan));
	$nama_belakang = escape(htmlentities($nama_belakang));
    $tgl_lahir = escape(htmlentities($tgl_lahir));
	$no_hp = escape(htmlentities($no_hp));

	$query = "INSERT INTO pekerja_bio (bio_id, nama_depan, nama_belakang, tgl_lahir, no_hp) VALUES ('','$nama_depan','$nama_belakang','$tgl_lahir', '$no_hp' )";
	return run($query);
}

function register_akun($biodata_id, $krywn_username, $krywn_email, $krywn_password, $krywn_jabatan, $krywn_role){
	$biodata_id = escape(htmlentities($biodata_id));
	$krywn_username = escape(htmlentities($krywn_username));
    $krywn_email = escape(htmlentities($krywn_email));
    $krywn_password = escape(htmlentities($krywn_password));
    $krywn_jabatan = escape(htmlentities($krywn_jabatan));
    $krywn_role = escape(htmlentities($krywn_role));
    $now = date("Y-m-d H:i:s");

    $ivlen=openssl_cipher_iv_length($cipher="AES-256-CBC");
    $iv=openssl_random_pseudo_bytes($ivlen);

    
    $secretHash = "1hwvyc28xmmy";

    $encrypted=openssl_encrypt($krywn_password, $cipher, $secretHash, 0, $iv);


    $query = "INSERT INTO pekerja_data (pekerja_id, krywn_bio, krywn_username, krywn_email, krywn_password, krywn_ivPass, krywn_jabatan, krywn_role, krywn_tgldftr) 
    VALUES ('', '$biodata_id', '$krywn_username', '$krywn_email', '$encrypted', '$iv', '$krywn_jabatan', '$krywn_role', '$now')";
	return run($query);
}

function register_cek($krywn_username){
	$krywn_username = escape($krywn_username);
	$query = "SELECT * FROM pekerja_data WHERE krywn_username='$krywn_username' ";

global $link;

	if ($result= mysqli_query($link, $query)) {
		if(mysqli_num_rows($result) == 0) return true;
		else return false;
	}
}

function cek_data($krywn_username, $krywn_password){
$krywn_username = escape(htmlentities($krywn_username));
$krywn_password = escape(htmlentities($krywn_password));

$ivlen=openssl_cipher_iv_length($cipher="AES-256-CBC");
$secretHash = "1hwvyc28xmmy";

$dekquery = "SELECT krywn_password, krywn_ivPass FROM pekerja_data WHERE krywn_username='$krywn_username'";
global $link;

	if ($hasil= mysqli_query($link, $dekquery)) {
		while($row=mysqli_fetch_assoc($hasil)) {
			$despass = $row['krywn_password'];
			$iv = $row['krywn_ivPass'];
		}

		$decrypted = openssl_decrypt($despass, $cipher, $secretHash, 0, $iv);

		if($krywn_password == $decrypted){
			if(mysqli_num_rows($hasil) != 0) return true;
			else return false;
		}

	}
}

function cek_level($krywn_usernameSession){
$krywn_usernameSession = escape(htmlentities($krywn_usernameSession));
$query = "SELECT krywn_role FROM pekerja_data WHERE krywn_username='$krywn_usernameSession' ";

global $link;

	if ($result= mysqli_query($link, $query)) {
		while($row=mysqli_fetch_assoc($result)) {
			$role = $row['krywn_role'];
				return $role;
			}

	}
}

function escape($data){
global $link;
return mysqli_real_escape_string($link, $data);
}

function run($query){
	global $link;

	if(mysqli_query($link,$query)) return true;
	else return false;
}

function result($query){
	global $link;
	if($result = mysqli_query($link, $query) or die('gagal menampilkan data')){

		return $result;
	}
}

?>
