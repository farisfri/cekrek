<?php

function tampilPelaksana(){
	// $query = "SELECT * FROM pelaksana";
	$query = "SELECT
			C.nama_depan AS nd_exe1, C.nama_belakang AS nb_exe1,
			E.nama_depan AS nd_exe2, E.nama_belakang AS nb_exe2,
			G.nama_depan AS nd_exe3, G.nama_belakang AS nb_exe3,
			exe_id
		FROM 
			pelaksana AS A 
			INNER JOIN pekerja_data AS B ON(A.exe_pelaksana1 = B.pekerja_id) 
			INNER JOIN pekerja_bio AS C ON(B.krywn_bio = C.bio_id)
			INNER JOIN pekerja_data AS D ON(A.exe_pelaksana2 = D.pekerja_id)
			INNER JOIN pekerja_bio AS E ON(D.krywn_bio = E.bio_id)
			INNER JOIN pekerja_data AS F ON(A.exe_pelaksana3 = F.pekerja_id)
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)";
	return result($query);
}

function tampilkan_per_pelaksana($exe_id){
	$query = "SELECT 
			C.nama_depan AS nd_exe1, C.nama_belakang AS nb_exe1,
			E.nama_depan AS nd_exe2, E.nama_belakang AS nb_exe2,
			G.nama_depan AS nd_exe3, G.nama_belakang AS nb_exe3,
			exe_id
		FROM 
			pelaksana AS A 
			INNER JOIN pekerja_data AS B ON(A.exe_pelaksana1 = B.pekerja_id) 
			INNER JOIN pekerja_bio AS C ON(B.krywn_bio = C.bio_id)
			INNER JOIN pekerja_data AS D ON(A.exe_pelaksana2 = D.pekerja_id)
			INNER JOIN pekerja_bio AS E ON(D.krywn_bio = E.bio_id)
			INNER JOIN pekerja_data AS F ON(A.exe_pelaksana3 = F.pekerja_id)
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)
		WHERE exe_id=$exe_id";
	return result($query);
}

function edit_pelaksana($username, $nama, $krywn_email, $no_hp, $kit_type, $role, $exe_id){
	$query="UPDATE pelaksana SET username='$username', nama='$nama', krywn_email='$krywn_email', no_hp='$no_hp', kit_type='$kit_type', role='$role'
	WHERE id=$id";

	return run($query);
}

function hapus_pelaksana($exe_id){
	$query="DELETE FROM pelaksana WHERE exe_id=$exe_id";
	return run($query);
}

function tambah_pelaksana($exe_pelaksana1, $exe_pelaksana2, $exe_pelaksana3){
	$exe_pelaksana1 = escape(htmlentities($exe_pelaksana1));
	$exe_pelaksana2 = escape(htmlentities($exe_pelaksana2));
    $exe_pelaksana3 = escape(htmlentities($exe_pelaksana3));


	$query = "INSERT INTO pelaksana (exe_id, exe_pelaksana1, exe_pelaksana2, exe_pelaksana3) VALUES ('','$exe_pelaksana1','$exe_pelaksana2','$exe_pelaksana3')";
	return run($query);
}