<?php

function tampilCare(){
	$query = "SELECT * FROM tipe_perbaikan";
	return result($query);
}

function tampilPerawatan(){
	$query = "SELECT
			prev_id, prev_mesinId, prev_startmtc, prev_endmtc, diff_startend, diff_before, prev_namaperbaikan, prev_keterangan, prev_catatanMec,
			prev_tipemtc, prev_statusMesin, prev_pelaksana, prev_resultStat, prev_priority,
			D.nama_depan AS nd_exe1, D.nama_belakang AS nb_exe1,
			G.nama_depan AS nd_exe2, G.nama_belakang AS nb_exe2,
			J.nama_depan AS nd_exe3, J.nama_belakang AS nb_exe3,
			K.mesin_id AS msn_Id_mesin, K.mesin_nama AS msn_nama_mesin, K.mesin_jenis AS msn_jenis_mesin,
			K.mesin_tipe AS msn_tipe_mesin, K.mesin_deskripsi AS msn_deskripsi_mesin,
			K.mesin_tglbeli AS msn_tglbeli_mesin,
			K.mesin_code AS msn_kode_mesin, K.mesin_blok AS msn_blok_mesin,
			K.mesin_line AS msn_line_mesin, K.mesin_nomor AS msn_nomor_mesin,
			L.tmtc_jenis AS tmtc_jenisPerbaikan, M.prio_keterangan AS pri_ket_priori

		FROM
			perawatan AS A
			INNER JOIN pelaksana AS B ON(A.prev_pelaksana = B.exe_id)
			INNER JOIN pekerja_data AS C ON(B.exe_pelaksana1 = C.pekerja_id)
			INNER JOIN pekerja_bio AS D ON(C.krywn_bio = D.bio_id)

			INNER JOIN pelaksana AS E ON(A.prev_pelaksana = E.exe_id)
			INNER JOIN pekerja_data AS F ON(E.exe_pelaksana2 = F.pekerja_id)
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)

			INNER JOIN pelaksana AS H ON(A.prev_pelaksana = H.exe_id)
			INNER JOIN pekerja_data AS I ON(H.exe_pelaksana3 = I.pekerja_id)
			INNER JOIN pekerja_bio AS J ON(I.krywn_bio = J.bio_id)

			INNER JOIN mesin_data AS K ON(A.prev_mesinId = K.mesin_id)
			INNER JOIN tipe_perbaikan AS L ON(A.prev_tipemtc = L.tmtc_id)
			INNER JOIN prioritas AS M ON(A.prev_priority = M.prio_id)";
	return result($query);
}

function tampilHistory($IdPerawatan, $IdTipePerbaikan){
	$query = "SELECT
			prev_id, prev_mesinId, prev_startmtc, prev_endmtc, diff_startend, diff_before, prev_namaperbaikan, prev_keterangan, prev_catatanMec,
			prev_tipemtc, prev_statusMesin, prev_pelaksana, prev_resultStat, prev_priority, DATEDIFF(prev_endmtc, prev_startmtc) AS DateDiff,
			D.nama_depan AS nd_exe1, D.nama_belakang AS nb_exe1,
			G.nama_depan AS nd_exe2, G.nama_belakang AS nb_exe2,
			J.nama_depan AS nd_exe3, J.nama_belakang AS nb_exe3,
			K.mesin_id AS msn_Id_mesin, K.mesin_nama AS msn_nama_mesin, K.mesin_jenis AS msn_jenis_mesin,
			K.mesin_tipe AS msn_tipe_mesin, K.mesin_deskripsi AS msn_deskripsi_mesin,
			K.mesin_tglbeli AS msn_tglbeli_mesin,
			K.mesin_code AS msn_kode_mesin, K.mesin_blok AS msn_blok_mesin,
			K.mesin_line AS msn_line_mesin, K.mesin_nomor AS msn_nomor_mesin,
			L.tmtc_jenis AS tmtc_jenisPerbaikan, M.prio_keterangan AS pri_ket_priori

		FROM
			perawatan AS A
			INNER JOIN pelaksana AS B ON(A.prev_pelaksana = B.exe_id)
			INNER JOIN pekerja_data AS C ON(B.exe_pelaksana1 = C.pekerja_id)
			INNER JOIN pekerja_bio AS D ON(C.krywn_bio = D.bio_id)

			INNER JOIN pelaksana AS E ON(A.prev_pelaksana = E.exe_id)
			INNER JOIN pekerja_data AS F ON(E.exe_pelaksana2 = F.pekerja_id)
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)

			INNER JOIN pelaksana AS H ON(A.prev_pelaksana = H.exe_id)
			INNER JOIN pekerja_data AS I ON(H.exe_pelaksana3 = I.pekerja_id)
			INNER JOIN pekerja_bio AS J ON(I.krywn_bio = J.bio_id)

			INNER JOIN mesin_data AS K ON(A.prev_mesinId = K.mesin_id)
			INNER JOIN tipe_perbaikan AS L ON(A.prev_tipemtc = L.tmtc_id)
			INNER JOIN prioritas AS M ON(A.prev_priority = M.prio_id)
			WHERE prev_mesinId=$IdPerawatan AND prev_tipemtc=$IdTipePerbaikan";
	return result($query);
}


function tampilHistoryDetail($IdPerawatan, $IdTipePerbaikan, $prev_mesinId_a){
	$query = "SELECT
			prev_id, prev_mesinId, prev_startmtc, prev_endmtc, diff_startend, diff_before, prev_namaperbaikan, prev_keterangan, prev_catatanMec,
			prev_tipemtc, prev_statusMesin, prev_pelaksana, prev_resultStat, prev_priority, DATEDIFF(prev_endmtc, prev_startmtc) AS DateDiff,
			D.nama_depan AS nd_exe1, D.nama_belakang AS nb_exe1,
			G.nama_depan AS nd_exe2, G.nama_belakang AS nb_exe2,
			J.nama_depan AS nd_exe3, J.nama_belakang AS nb_exe3,
			K.mesin_id AS msn_Id_mesin, K.mesin_nama AS msn_nama_mesin, K.mesin_jenis AS msn_jenis_mesin,
			K.mesin_tipe AS msn_tipe_mesin, K.mesin_deskripsi AS msn_deskripsi_mesin,
			K.mesin_tglbeli AS msn_tglbeli_mesin,
			K.mesin_code AS msn_kode_mesin, K.mesin_blok AS msn_blok_mesin,
			K.mesin_line AS msn_line_mesin, K.mesin_nomor AS msn_nomor_mesin,
			L.tmtc_jenis AS tmtc_jenisPerbaikan, M.prio_keterangan AS pri_ket_priori

		FROM
			perawatan AS A
			INNER JOIN pelaksana AS B ON(A.prev_pelaksana = B.exe_id)
			INNER JOIN pekerja_data AS C ON(B.exe_pelaksana1 = C.pekerja_id)
			INNER JOIN pekerja_bio AS D ON(C.krywn_bio = D.bio_id)

			INNER JOIN pelaksana AS E ON(A.prev_pelaksana = E.exe_id)
			INNER JOIN pekerja_data AS F ON(E.exe_pelaksana2 = F.pekerja_id)
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)

			INNER JOIN pelaksana AS H ON(A.prev_pelaksana = H.exe_id)
			INNER JOIN pekerja_data AS I ON(H.exe_pelaksana3 = I.pekerja_id)
			INNER JOIN pekerja_bio AS J ON(I.krywn_bio = J.bio_id)

			INNER JOIN mesin_data AS K ON(A.prev_mesinId = K.mesin_id)
			INNER JOIN tipe_perbaikan AS L ON(A.prev_tipemtc = L.tmtc_id)
			INNER JOIN prioritas AS M ON(A.prev_priority = M.prio_id)
			WHERE prev_mesinId=$prev_mesinId_a AND prev_tipemtc=$IdTipePerbaikan";
	return result($query);
}
// function{
// 	Mulai Perawatan
// SELECT prev_endmtc, NOW(), DATEDIFF(NOW(), prev_endmtc) AS mulai_sel FROM perawatan ORDER BY prev_id DESC LIMIT 0,1

// Antara 2 Tanggal
// SELECT DATEDIFF(prev_endmtc, prev_startmtc) AS DateDiff FROM perawatan WHERE 2;

// Dash-Scan Perawatan
// SELECT * FROM perawatan WHERE prev_mesinId=3 AND prev_tipemtc=4;

// }


function tampilkan_per_perawatan($IdPerawatan, $IdTipePerbaikan){
	$query = "SELECT
			prev_id, prev_mesinId, prev_startmtc, prev_endmtc, diff_startend, diff_before, prev_namaperbaikan, prev_keterangan, prev_catatanMec,
			prev_tipemtc, prev_statusMesin, prev_pelaksana, prev_resultStat, prev_priority,
			D.nama_depan AS nd_exe1, D.nama_belakang AS nb_exe1,
			G.nama_depan AS nd_exe2, G.nama_belakang AS nb_exe2,
			J.nama_depan AS nd_exe3, J.nama_belakang AS nb_exe3,
			K.mesin_id AS msn_Id_mesin, K.mesin_nama AS msn_nama_mesin, K.mesin_jenis AS msn_jenis_mesin,
			K.mesin_tipe AS msn_tipe_mesin, K.mesin_deskripsi AS msn_deskripsi_mesin,
			K.mesin_tglbeli AS msn_tglbeli_mesin,
			K.mesin_code AS msn_kode_mesin, K.mesin_blok AS msn_blok_mesin,
			K.mesin_line AS msn_line_mesin, K.mesin_nomor AS msn_nomor_mesin,
			L.tmtc_jenis AS tmtc_jenisPerbaikan, M.prio_keterangan AS pri_ket_priori

		FROM
			perawatan AS A
			INNER JOIN pelaksana AS B ON(A.prev_pelaksana = B.exe_id)
			INNER JOIN pekerja_data AS C ON(B.exe_pelaksana1 = C.pekerja_id)
			INNER JOIN pekerja_bio AS D ON(C.krywn_bio = D.bio_id)

			INNER JOIN pelaksana AS E ON(A.prev_pelaksana = E.exe_id)
			INNER JOIN pekerja_data AS F ON(E.exe_pelaksana2 = F.pekerja_id)
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)

			INNER JOIN pelaksana AS H ON(A.prev_pelaksana = H.exe_id)
			INNER JOIN pekerja_data AS I ON(H.exe_pelaksana3 = I.pekerja_id)
			INNER JOIN pekerja_bio AS J ON(I.krywn_bio = J.bio_id)

			INNER JOIN mesin_data AS K ON(A.prev_mesinId = K.mesin_id)
			INNER JOIN tipe_perbaikan AS L ON(A.prev_tipemtc = L.tmtc_id)
			INNER JOIN prioritas AS M ON(A.prev_priority = M.prio_id)
			WHERE prev_mesinId=$IdPerawatan AND prev_tipemtc=$IdTipePerbaikan
			ORDER BY prev_id DESC LIMIT 1";
	return result($query);
}

function tampilkan_per_perawatanDetail($IdPerawatan, $IdTipePerbaikan){
	$query = "SELECT
			prev_id, prev_mesinId, prev_startmtc, prev_endmtc, diff_startend, diff_before, prev_namaperbaikan, prev_keterangan, prev_catatanMec,
			prev_tipemtc, prev_statusMesin, prev_pelaksana, prev_resultStat, prev_priority,
			D.nama_depan AS nd_exe1, D.nama_belakang AS nb_exe1,
			G.nama_depan AS nd_exe2, G.nama_belakang AS nb_exe2,
			J.nama_depan AS nd_exe3, J.nama_belakang AS nb_exe3,
			K.mesin_id AS msn_Id_mesin, K.mesin_nama AS msn_nama_mesin, K.mesin_jenis AS msn_jenis_mesin,
			K.mesin_tipe AS msn_tipe_mesin, K.mesin_deskripsi AS msn_deskripsi_mesin,
			K.mesin_tglbeli AS msn_tglbeli_mesin,
			K.mesin_code AS msn_kode_mesin, K.mesin_blok AS msn_blok_mesin,
			K.mesin_line AS msn_line_mesin, K.mesin_nomor AS msn_nomor_mesin,
			L.tmtc_jenis AS tmtc_jenisPerbaikan, M.prio_keterangan AS pri_ket_priori

		FROM
			perawatan AS A
			INNER JOIN pelaksana AS B ON(A.prev_pelaksana = B.exe_id)
			INNER JOIN pekerja_data AS C ON(B.exe_pelaksana1 = C.pekerja_id)
			INNER JOIN pekerja_bio AS D ON(C.krywn_bio = D.bio_id)

			INNER JOIN pelaksana AS E ON(A.prev_pelaksana = E.exe_id)
			INNER JOIN pekerja_data AS F ON(E.exe_pelaksana2 = F.pekerja_id)
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)

			INNER JOIN pelaksana AS H ON(A.prev_pelaksana = H.exe_id)
			INNER JOIN pekerja_data AS I ON(H.exe_pelaksana3 = I.pekerja_id)
			INNER JOIN pekerja_bio AS J ON(I.krywn_bio = J.bio_id)

			INNER JOIN mesin_data AS K ON(A.prev_mesinId = K.mesin_id)
			INNER JOIN tipe_perbaikan AS L ON(A.prev_tipemtc = L.tmtc_id)
			INNER JOIN prioritas AS M ON(A.prev_priority = M.prio_id)
			WHERE prev_id=$IdPerawatan AND prev_tipemtc=$IdTipePerbaikan";
	return result($query);
}

function notif_perawatan($mesin_idDetail){
	$query = "SELECT DISTINCT
			K.mesin_nama AS msn_nama_mesin,
			L.tmtc_jenis AS tmtc_jenisPerbaikan

		FROM
			perawatan AS A

			INNER JOIN mesin_data AS K ON(A.prev_mesinId = K.mesin_id)
			INNER JOIN tipe_perbaikan AS L ON(A.prev_tipemtc = L.tmtc_id)
			WHERE prev_mesinId=$mesin_idDetail";
	return result($query);
}

//Pencarian
function hasil_cariFilter($mesin_idDetail, $filter_tipe_perbaikan){
	$query = "SELECT DISTINCT
			K.mesin_nama AS msn_nama_mesin,
			L.tmtc_jenis AS tmtc_jenisPerbaikan

		FROM
			perawatan AS A

			INNER JOIN mesin_data AS K ON(A.prev_mesinId = K.mesin_id)
			INNER JOIN tipe_perbaikan AS L ON(A.prev_tipemtc = L.tmtc_id)
			WHERE prev_mesinId LIKE '%$mesin_idDetail%' AND prev_tipemtc LIKE '%$filter_tipe_perbaikan%'";
	return result($query);
}

function mulai_perawatan($prev_mesinId_a, $prev_endmtc_a, $prev_namaperbaikan_a, $prev_tipemtc_a, $prev_statusMesin_a, $prev_pelaksana_a, $prev_priority_a){
	$prev_mesinId = escape(htmlentities($prev_mesinId_a));
	$prev_endmtc = escape(htmlentities($prev_endmtc_a));
	$prev_namaperbaikan = escape(htmlentities($prev_namaperbaikan_a));
	$prev_tipemtc = escape(htmlentities($prev_tipemtc_a));
	$prev_statusMesin = escape(htmlentities($prev_statusMesin_a));
	$prev_pelaksana = escape(htmlentities($prev_pelaksana_a));
	$prev_priority = escape(htmlentities($prev_priority_a));
	$now = date("Y-m-d");

	$query="INSERT INTO perawatan (prev_id, prev_mesinId, prev_startmtc, prev_namaperbaikan, prev_tipemtc, prev_statusMesin, prev_pelaksana, prev_resultStat, prev_priority) 
	VALUES ('','$prev_mesinId','$prev_endmtc','$prev_namaperbaikan','$prev_tipemtc',2,'$prev_pelaksana',1,'$prev_priority')";

	return run($query);
}

function update_perawatan($prev_startmtc_a, $prev_endmtc_a, $prev_keterangan, $prev_catatanMec, $prev_id_a){
	$prev_id = escape(htmlentities($prev_id_a));
	$prev_startmtc = escape(htmlentities($prev_startmtc_a));
	$prev_endmtc = escape(htmlentities($prev_endmtc_a));
	$prev_keterangan = escape(htmlentities($prev_keterangan));
	$prev_catatanMec = escape(htmlentities($prev_catatanMec));
	$now = date("Y-m-d");

	$date1=date_create("$prev_startmtc_a");
    $date2=date_create("$now");
    $diff=date_diff($date1,$date2);
    $strDays=$diff->days;

	$query="UPDATE perawatan SET prev_endmtc='$now', diff_startend='$strDays', prev_keterangan='$prev_keterangan', prev_catatanMec='$prev_catatanMec', prev_statusMesin=1, prev_resultStat=3
	WHERE prev_id=$prev_id";

	return run($query);
}

function selesai_perawatan($prev_mesinId_a, $prev_startmtc_a, $prev_endmtc_a, $prev_namaperbaikan_a, $prev_keterangan_a, $prev_tipemtc_a, $prev_catatanMec_a, $prev_pelaksana_a, $prev_priority_a, $prev_id_a){
	$prev_id = escape(htmlentities($prev_id_a));
	$prev_mesinId = escape(htmlentities($prev_mesinId_a));
	$prev_startmtc = escape(htmlentities($prev_startmtc_a));
	$prev_endmtc = escape(htmlentities($prev_endmtc_a));
	$prev_namaperbaikan = escape(htmlentities($prev_namaperbaikan_a));
	$prev_keterangan = escape(htmlentities($prev_keterangan_a));
	$prev_catatanMec = escape(htmlentities($prev_catatanMec_a));
	$prev_tipemtc = escape(htmlentities($prev_tipemtc_a));
	$prev_pelaksana = escape(htmlentities($prev_pelaksana_a));
	$prev_priority = escape(htmlentities($prev_priority_a));
	$now = date("Y-m-d");

	$date1=date_create("$prev_endmtc");
    $date2=date_create("$now");
    $diff=date_diff($date1,$date2);
    $strDays=$diff->days;

	$query="INSERT INTO perawatan (prev_id, prev_mesinId, prev_startmtc, prev_endmtc, diff_startend, prev_namaperbaikan, prev_tipemtc, prev_keterangan, prev_catatanMec, prev_statusMesin, prev_pelaksana, prev_resultStat, prev_priority) 
	VALUES ('','$prev_mesinId','$now','$now','$strDays','$prev_namaperbaikan','$prev_tipemtc','$prev_keterangan','$prev_catatanMec',2,'$prev_pelaksana',3,'$prev_priority')";

	return run($query);
}

function rilis_mtbf($IdPerawatan, $IdTipePerbaikan, $prev_endmtc_a){
	$IdPerawatan = escape(htmlentities($IdPerawatan));
	$IdTipePerbaikan = escape(htmlentities($IdTipePerbaikan));

	global $link;

	$query1="SELECT COUNT(diff_startend) as count FROM perawatan WHERE prev_mesinId=$IdPerawatan AND prev_tipemtc=$IdTipePerbaikan AND prev_statusMesin=1";
	$query2="SELECT SUM(diff_startend) as total FROM perawatan WHERE prev_mesinId=$IdPerawatan AND prev_tipemtc=$IdTipePerbaikan AND prev_statusMesin=1";

	if ($hasil= mysqli_query($link, $query1)) {
		while($row=mysqli_fetch_assoc($hasil)) {
			$jumlahkerusakan = $row['count'];
		}
	}

	if ($hasil= mysqli_query($link, $query2)) {
		while($row1=mysqli_fetch_assoc($hasil)) {
			$totalhari = $row1['total'];
		}

	}

	  $mtbf=$totalhari/$jumlahkerusakan;
      $mean=round($mtbf);

      $date_start = $prev_endmtc_a;
      $date_rekom = date('Y-m-d', strtotime($date_start. ' + '.$mean.' days'));

      return $mean;
}

function rilis_mttr($IdPerawatan, $IdTipePerbaikan, $prev_endmtc_a){
	$IdPerawatan = escape(htmlentities($IdPerawatan));
	$IdTipePerbaikan = escape(htmlentities($IdTipePerbaikan));

	global $link;

	$query1="SELECT COUNT(diff_startend) as count FROM perawatan WHERE prev_mesinId=$IdPerawatan AND prev_tipemtc=$IdTipePerbaikan AND prev_statusMesin=2";
	$query2="SELECT SUM(diff_startend) as total FROM perawatan WHERE prev_mesinId=$IdPerawatan AND prev_tipemtc=$IdTipePerbaikan AND prev_statusMesin=2";

	if ($hasil= mysqli_query($link, $query1)) {
		while($row=mysqli_fetch_assoc($hasil)) {
			$jumlahkerusakan = $row['count'];
		}
	}

	if ($hasil= mysqli_query($link, $query2)) {
		while($row1=mysqli_fetch_assoc($hasil)) {
			$totalhari = $row1['total'];
		}

	}

	  $mttr=$totalhari/$jumlahkerusakan;
      $mean=round($mttr);

      $date_start = $prev_endmtc_a;
      $date_rekom = date('Y-m-d', strtotime($date_start. ' + '.$mean.' days'));

      return $mean;
}
