<?php

function tampilPerintah(){
	$query = "SELECT
			perintah_id, perintah_mesinId, perintah_judul, 
			perintah_keterangan, perintah_catatan, perintah_crtuser, perintah_pelaksanaId, 
			perintah_crtdate, perintah_prioritas,
			D.nama_depan AS nd_exe1, D.nama_belakang AS nb_exe1,
			G.nama_depan AS nd_exe2, G.nama_belakang AS nb_exe2,
			J.nama_depan AS nd_exe3, J.nama_belakang AS nb_exe3,
			K.mesin_code AS prt_kode_mesin, K.mesin_blok AS prt_blok_mesin, 
			K.mesin_line AS prt_line_mesin, K.mesin_nomor AS prt_nomor_mesin,
			L.prio_keterangan AS prt_ket_priori, N.nama_depan AS prt_namdep_hmd,
			N.nama_belakang AS prt_nambel_hmd

		FROM perintah AS A
		INNER JOIN pelaksana AS B ON(A.perintah_pelaksanaId=B.exe_id)
		INNER JOIN pekerja_data AS C ON(B.exe_pelaksana1=C.pekerja_id)
		INNER JOIN pekerja_bio AS D ON(C.pekerja_id=D.bio_id)
		
		INNER JOIN pelaksana AS E ON(A.perintah_pelaksanaId =E.exe_id) 
		INNER JOIN pekerja_data AS F ON(E.exe_pelaksana2 = F.pekerja_id) 
		INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)
			
		INNER JOIN pelaksana AS H ON(A.perintah_pelaksanaId = H.exe_id) 
		INNER JOIN pekerja_data AS I ON(H.exe_pelaksana3 = I.pekerja_id) 
		INNER JOIN pekerja_bio AS J ON(I.krywn_bio = J.bio_id)
		
		INNER JOIN mesin_data AS K ON(A.perintah_mesinId = K.mesin_id)
		INNER JOIN prioritas AS L ON(A.perintah_prioritas = L.prio_id)
		INNER JOIN pekerja_data AS M ON(A.perintah_crtuser = M.pekerja_id)
		INNER JOIN pekerja_bio AS N ON(M.pekerja_id = N.bio_id)";
	return result($query);
}

function tampilPrioritas(){
	$query = "SELECT * FROM prioritas";
	return result($query);
}

function tampilkan_per_perintah($perintah_id){
	$query = "SELECT
			perintah_id, perintah_mesinId, perintah_judul, 
			perintah_keterangan, perintah_catatan, perintah_crtuser, 
			perintah_crtdate, perintah_prioritas, perintah_pelaksanaId,
			D.nama_depan AS nd_exe1, D.nama_belakang AS nb_exe1,
			G.nama_depan AS nd_exe2, G.nama_belakang AS nb_exe2,
			J.nama_depan AS nd_exe3, J.nama_belakang AS nb_exe3, 
			K.mesin_nama AS prt_nama_mesin, K.mesin_jenis AS prt_jenis_mesin, 
			K.mesin_tipe AS prt_tipe_mesin, K.mesin_deskripsi AS prt_deskripsi_mesin,
			K.mesin_tglbeli AS prt_tglbeli_mesin,
			K.mesin_code AS prt_kode_mesin, K.mesin_blok AS prt_blok_mesin, 
			K.mesin_line AS prt_line_mesin, K.mesin_nomor AS prt_nomor_mesin,
			L.prio_keterangan AS prt_ket_priori, N.nama_depan AS prt_namdep_hmd,
			N.nama_belakang AS prt_nambel_hmd
		FROM 
			perintah AS A
			INNER JOIN pelaksana AS B ON(A.perintah_pelaksanaId = B.exe_id) 
			INNER JOIN pekerja_data AS C ON(B.exe_pelaksana1 = C.pekerja_id) 
			INNER JOIN pekerja_bio AS D ON(C.krywn_bio = D.bio_id)
			
			INNER JOIN pelaksana AS E ON(A.perintah_pelaksanaId = E.exe_id) 
			INNER JOIN pekerja_data AS F ON(E.exe_pelaksana2 = F.pekerja_id) 
			INNER JOIN pekerja_bio AS G ON(F.krywn_bio = G.bio_id)
			
			INNER JOIN pelaksana AS H ON(A.perintah_pelaksanaId = H.exe_id) 
			INNER JOIN pekerja_data AS I ON(H.exe_pelaksana3 = I.pekerja_id) 
			INNER JOIN pekerja_bio AS J ON(I.krywn_bio = J.bio_id)
			
			INNER JOIN mesin_data AS K ON(A.perintah_mesinId = K.mesin_id)
			INNER JOIN prioritas AS L ON(A.perintah_prioritas = L.prio_id)
			INNER JOIN pekerja_data AS M ON(A.perintah_crtuser = M.pekerja_id)
			INNER JOIN pekerja_bio AS N ON(M.pekerja_id = N.bio_id) WHERE perintah_id=$perintah_id";
	return result($query);
}

function edit_perintah($username, $nama, $krywn_email, $no_hp, $kit_type, $role, $id){
	$query="UPDATE perintah SET username='$username', nama='$nama', krywn_email='$krywn_email', no_hp='$no_hp', kit_type='$kit_type', role='$role'
	WHERE id=$id";

	return run($query);
}

function hapus_perintah($id){
	$query="DELETE FROM perintah WHERE id=$id";
	return run($query);
}

function tambah_perintah($mesin_id, $perintah_judul, $perintah_keterangan, $perintah_catatan, $perintah_crtuser, $exe_id, $perintah_prioritas){
	$mesin_id = escape(htmlentities($mesin_id));
	$perintah_judul = escape(htmlentities($perintah_judul));
	$perintah_crtuser = escape(htmlentities($perintah_crtuser));
	$exe_id = escape(htmlentities($exe_id));
	$perintah_prioritas = escape(htmlentities($perintah_prioritas));
	$now = date("Y-m-d");

	$query = "INSERT INTO perintah (perintah_id, perintah_mesinId, perintah_judul, perintah_keterangan, perintah_catatan, perintah_crtuser, perintah_pelaksanaId, perintah_crtdate, perintah_prioritas) VALUES ('','$mesin_id','$perintah_judul','$perintah_keterangan','$perintah_catatan','$perintah_crtuser','$exe_id','$now','$perintah_prioritas')";
	return run($query);
}

function rilis_perbaikan($perintah_id_lihat){
	$query = "INSERT INTO perbaikan (mtc_mesinId, mtc_perintahId, mtc_statusMesin, mtc_crtuser, mtc_pelaksana, mtc_resultStat, mtc_prioritas) SELECT perintah_mesinId,perintah_id,0,perintah_crtuser,perintah_pelaksanaId,0,perintah_prioritas FROM perintah WHERE perintah_id=$perintah_id_lihat";
	return run($query);
}