<?php
require_once "core/init.php";

//set session true/false
if(isset($_SESSION['krywn_usernameSession'])) {
    header('Location: sim/index.php');
}else{

$error ='';

if(isset($_POST['submit'])){
    $krywn_username = $_POST['krywn_username'];
    $krywn_password = $_POST['krywn_password'];

    if(!empty(trim($krywn_username)) && !empty(trim($krywn_password))){
        if(register_cek($krywn_username)){
          $error='<div class="alert alert-danger" role="alert">Username tidak ada</div>';
        }else{
          if(cek_data($krywn_username, $krywn_password)){
              $_SESSION['krywn_usernameSession'] = $krywn_username;

              header('Location: sim/index.php');
          }else{
              $error='<div class="alert alert-danger" role="alert">Periksa kembali username dan password anda</div>';
          }
        }
    }else{
        $error = '<div class="alert alert-danger" role="alert">Username dan password wajib diisi</div>';
    }
}


require_once"layout/header.php";
?>


<section id="login-page">
  <!-- Start your project here-->
  <div class="container-fluid">
    <!-- Card -->
    <div class="card login-card mx-auto">
      <div class="card-body px-md-3 pt-2">
        <h3 class="pt-2 text-center"><strong>CekRek Apps</strong></h3>
        <form class="form-group-no-border pt-2" method="post" action="">
          <!--Body-->
            <div class="md-form">
              <input type="text" id="krywn_username" name="krywn_username" class="form-control">
              <label for="krywn_username">Your Username</label>
            </div>
            <div class="md-form">
              <input type="password" id="krywn_password" name="krywn_password" class="form-control">
              <label for="krywn_password">Your password</label>
            </div>
            <?= $error ?>
            <button class="btn btn btn-primary btn-rounded btn-block my-1 waves-effect z-depth-0" type="submit" name="submit">Sign in <i class="fa fa-sign-in ml-1"></i></button>
        </form>
        <!-- Card -->
      </div>
  </div>
</section>
<!-- /Start your project here-->


<?php require_once "layout/footer.php" ?>

<?php } ?>
