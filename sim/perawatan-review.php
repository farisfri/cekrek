<?php

require_once "core/init.php";

$IdPerawatan = $_GET['IdPerawatan'];
$IdTipePerbaikan = $_GET['IdTipePerbaikan'];

if(isset($_GET['IdPerawatan'])){

    $perawatan= tampilkan_per_perawatan($IdPerawatan, $IdTipePerbaikan);
    while($row=mysqli_fetch_assoc($perawatan)){
        		      $prev_id_a = $row['prev_id'];
                  $prev_mesinId_a = $row['prev_mesinId'];
                  $prev_startmtc_a = $row['prev_startmtc'];
                  $prev_endmtc_a = $row['prev_endmtc'];
                  $diff_startend_a = $row['diff_startend'];
                  $diff_before_a = $row['diff_before'];
                  $prev_namaperbaikan_a = $row['prev_namaperbaikan'];
                  $prev_keterangan_a = $row['prev_keterangan'];
                  $prev_catatanMec_a = $row['prev_catatanMec'];
                  $prev_tipemtc_a = $row['prev_tipemtc'];
                  $prev_statusMesin_a= $row['prev_statusMesin'];
                  $prev_pelaksana_a= $row['prev_pelaksana'];
                  $prev_resultStat_a= $row['prev_resultStat'];
                  $prev_priority_a= $row['prev_priority'];
                  $msn_Id_mesin_a= $row['msn_Id_mesin'];
                  $msn_nama_mesin_a= $row['msn_nama_mesin'];
                  $msn_jenis_mesin_a= $row['msn_jenis_mesin'];
                  $msn_tipe_mesin_a= $row['msn_tipe_mesin'];
                  $msn_deskripsi_mesin_a= $row['msn_deskripsi_mesin'];
                  $msn_tglbeli_mesin_a= $row['msn_tglbeli_mesin'];
                  $msn_kode_mesin_a= $row['msn_kode_mesin'];
                  $msn_blok_mesin_a= $row['msn_blok_mesin'];
                  $msn_line_mesin_a= $row['msn_line_mesin'];
                  $msn_nomor_mesin_a=$row['msn_nomor_mesin'];
                  $perintah_nd_exe1_a= $row['nd_exe1'];
                  $perintah_nb_exe1_a=$row['nb_exe1'];
                  $perintah_nd_exe2_a= $row['nd_exe2'];
                  $perintah_nb_exe2_a=$row['nb_exe2'];
                  $perintah_nd_exe3_a= $row['nd_exe3'];
                  $perintah_nb_exe3_a=$row['nb_exe3'];
                  $tmtc_jenisPerbaikan_a= $row['tmtc_jenisPerbaikan'];
                  $pri_ket_priori_a=$row['pri_ket_priori'];
    }
      $MTBF=rilis_mtbf($IdPerawatan, $IdTipePerbaikan , $prev_endmtc_a);

      $mean=$MTBF;

      $date_start = $prev_endmtc_a;
      $date_rekom = date('Y-m-d', strtotime($date_start. ' + '.$mean.' days'));

      $MTTR=rilis_mttr($IdPerawatan, $IdTipePerbaikan, $prev_endmtc_a);

      $mean1=$MTTR;

      $date_start = $prev_endmtc_a;
      $date_rekom = date('Y-m-d', strtotime($date_start. ' + '.$mean1.' days'));
}

$error ='';
if(isset($_POST['submit'])){
        if(mulai_perawatan($prev_mesinId_a, $prev_endmtc_a, $prev_namaperbaikan_a, $prev_tipemtc_a, $prev_statusMesin_a, $prev_pelaksana_a, $prev_priority_a)){
            $prev_id_lanjut=$prev_id_a+1;
            header('location: perawatan-mulai.php'."?IdPerawatan=".$prev_id_lanjut."&IdTipePerbaikan=".$prev_tipemtc_a);
        }else{
            $error='ada masalah saat menambah data';
        }
}

$semuaHistory = tampilHistory($IdPerawatan, $IdTipePerbaikan);

require_once "layout/head.php";
require_once "view/perawatan-review.php";
require_once "layout/footer.php";

?>
