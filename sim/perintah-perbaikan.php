<?php

require_once "core/init.php";

$mesin_id = $_GET['mesin_id'];
$exe_id = $_GET['exe_id'];

if(isset($_SESSION['krywn_usernameSession'])) {
    $usid = $_SESSION['krywn_usernameSession'];
    $usRef = tampilUsername($usid);
    while($row=mysqli_fetch_assoc($usRef)){
        $perintah_crtuser = $row['pekerja_id'];
    }  
}

$semuaPrioritas = tampilPrioritas();

if(isset($_GET['exe_id'])){

	$pelaksanaData =tampilkan_per_pelaksana($exe_id);
	while($row=mysqli_fetch_assoc($pelaksanaData)){
		$exe_id = $row['exe_id'];
		$nd_exe1 = $row['nd_exe1'];
		$nb_exe1 = $row['nb_exe1'];
		$nd_exe2 = $row['nd_exe2'];
		$nb_exe2 = $row['nb_exe2'];
		$nd_exe3 = $row['nd_exe3'];
		$nb_exe3 = $row['nb_exe3'];
	}
}

if(isset($_GET['mesin_id'])){

	$mesinData =tampilkan_detail_mesin($mesin_id);
	while($row=mysqli_fetch_assoc($mesinData)){
		$mesin_id = $row['mesin_id'];
		$mesin_nama = $row['mesin_nama'];
		$mesin_jenis = $row['mesin_jenis'];
		$mesin_tipe = $row['mesin_tipe'];
        $mesin_deskripsi = $row['mesin_deskripsi'];
        $mesin_tglbeli = $row['mesin_tglbeli'];
        $mesin_blok = $row['mesin_blok'];
		$mesin_line = $row['mesin_line'];
		$mesin_nomor = $row['mesin_nomor'];
	}
}

$error ='';
if(isset($_POST['submit'])){
    $perintah_judul = $_POST['perintah_judul'];
    $perintah_keterangan = $_POST['perintah_keterangan'];
    $perintah_catatan = $_POST['perintah_catatan'];
    $perintah_prioritas = $_POST['perintah_prioritas'];


    if(!empty(trim($perintah_judul))){
        if(tambah_perintah($mesin_id, $perintah_judul, $perintah_keterangan, $perintah_catatan, $perintah_crtuser, $exe_id, $perintah_prioritas)){
            header('location: perintah-daftar.php');
        }else{
            $error='ada masalah saat menambah data';
        }

    }else{
       $error = 'judul dan konten wajib diisi';
	}
}

require_once "layout/head.php";
require_once "view/perintah-perbaikan.php";
require_once "layout/footer.php";

?>