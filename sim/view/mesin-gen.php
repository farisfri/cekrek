  <?php 
    include "../vendor/phpqrcode/qrlib.php";

    $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
    
    //html PNG location prefix
    $PNG_WEB_DIR = 'temp/';
  ?>

  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="mesin-data.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">GENERATE MACHINE CODE</h5>
            </div>
          </div>

          <div class="container-fluid mt-5">
            <?php 

            //ofcourse we need rights to create temp dir
            if (!file_exists($PNG_TEMP_DIR))
                mkdir($PNG_TEMP_DIR);
            
            $filename = $PNG_TEMP_DIR;

            $errorCorrectionLevel = 'H';
            if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
                $errorCorrectionLevel = $_REQUEST['level'];    

            $matrixPointSize = 6;
            if (isset($_REQUEST['size']))
                $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);


            if (isset($_REQUEST['kodeGenerator'])) { 
            
                //it's very important!
                if (trim($_REQUEST['kodeGenerator']) == '')
                    die('data cannot be empty! <a href="?">back</a>');
                    
                // user data
                $filename = $PNG_TEMP_DIR.'test'.md5($_REQUEST['kodeGenerator'].'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
                QRcode::png($_REQUEST['kodeGenerator'], $filename, $errorCorrectionLevel, $matrixPointSize, 2);  

                $error ='';
                if(isset($_POST['submit'])){
                  $mesin_code = $_REQUEST['kodeGenerator'];


                  $mesin_qrcode = $PNG_WEB_DIR.basename($filename);

                  if(!empty(trim($mesin_code))){

                      if(register_kodeqrMesin($mesin_code, $mesin_qrcode, $mesinData_id)){
                           
                      }else{
                          $error='ada masalah saat menambah data';
                      }

                  }else{
                    $error = 'judul dan konten wajib diisi';
                  }
                } 

                echo '<img class="mx-auto d-block" src="view/'.$PNG_WEB_DIR.basename($filename).'" />';  
                
            } else {    
            
                 echo '<img class="mx-auto d-block" src="../assets/img/dash/qr-code.png" style="width: 100px; height: 100px;">';          
                
            }    

            //display generated file
             
            
            //config form
            echo '<form action="" method="post" class="mt-5" >
                <div class="input-group mb-3 mx-auto" style="width: 30%;">
                <input type="text" class="form-control" name="kodeGenerator" placeholder="Enter Machine Code..." aria-label="Enter Machine Code..."
                    aria-describedby="buttonGenerate" value="'.(isset($_REQUEST['kodeGenerator'])?htmlspecialchars($_REQUEST['kodeGenerator']):'').'" />
               	<div class="input-group-append">
                <button class="btn btn-md btn-primary m-0 px-3 py-2 z-depth-0 waves-effect" type="submit" name="submit" id="buttonGenerate">GENERATE</button></div></div>';
             ?>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->