

  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="pekerja-data.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">EDIT ACCOUNT</h5>
            </div>
          </div>

          <div class="container-fluid mt-5">
            <div class="form-row ">
                <div class="col">
                    <!-- First name -->
                    <label for="krywn_username">Username</label>
                    <input type="text" id="krywn_username" class="form-control validate" name="krywn_username">
                </div>
                <div class="col">
                    <!-- Last name -->
                    <label for="krywn_password">Password</label>
                    <input type="password" id="krywn_password" class="form-control validate" name="krywn_password">
                </div>
            </div>

            <div class="form-row mt-2">
              <div class="col">
                <label for="krywn_email">Email</label>
                <input type="email" id="krywn_email" class="form-control validate" name="krywn_email">
              </div>
              <div class="col">
                 <label for="krywn_jabatan">Job Title</label>
                <input type="email" id="krywn_jabatan" class="form-control validate" name="krywn_jabatan">
              </div>
              <div class="col">
                <label for="custom-select-role">Role User</label>
                <select id="custom-select-role" class="form-control browser-default custom-select-role" style="width: 100%;">
                  <option selected>-- Choose Role --</option>
                  <option value="0">Super Admin</option>
                  <option value="1">Head Maintenance Division</option>
                  <option value="2">Mechanic</option>
               </select>
              </div>
            </div>
            <a type="button" class="btn btn-success waves-effect mt-4 net-mr float-right">SAVE <i class="fas fa-paper-plane-o ml-1"></i></a>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->