  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="pekerja-data.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>
              <a href="" class="btn btn-success" style="margin-top: 8px;" data-toggle="modal" data-target="#tambahBiodata">
                Create Group
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">INVITE MECHANIC</h5>
            </div>
          </div>

          <div class="container-fluid mt-2">
            <table id="dtHorizontalExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="th-sm">No.
                  </th>
                  <th class="th-sm">Worker 1
                  </th>
                  <th class="th-sm">Worker 2
                  </th>
                  <th class="th-sm">Worker 3
                  </th>
                  <th class="th-sm">Option
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php while($row=mysqli_fetch_assoc($semuaPelaksana)):  ?>
                <tr>
                  <td><?= $row['exe_id']; ?></td>
                  <td><?= $row['nd_exe1']." ".$row['nb_exe1']; ?></td>
                  <td><?= $row['nd_exe2']." ".$row['nb_exe2']; ?></td>
                  <td><?= $row['nd_exe3']." ".$row['nb_exe3']; ?></td>
                  <td><a href="perintah-perbaikan.php?mesin_id=<?= $mesin_id; ?>&exe_id=<?= $row['exe_id']; ?>" class="btn btn-success btn-sm btn-tabel" data-toggle="tooltip" title="Pilih Kelompok"><i class="fas fa-arrow-right"></i></a>
                      <a href="" class="btn btn-danger btn-sm btn-tabel"><i class="far fa-trash-alt" data-toggle="tooltip" title="Hapus Data"></i></a>
                  </td>
                </tr>
                <?php endwhile; ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No.
                  </th>
                  <th>Worker 1
                  </th>
                  <th>Worker 2
                  </th>
                  <th>Worker 3
                  </th>
                  <th>Option
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->

  <form action="" method="post" enctype="multipart/form-data">

  <div class="modal fade" id="tambahBiodata" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-success" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2">Create Group</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <label for="pelaksana1">Worker 1</label>
          <select id="pelaksana1" name="exe_pelaksana1" class="form-control browser-default custom-select-role" style="width: 100%;">
              <option value="0" selected disabled="">-- Worker 1 --</option>
            <?php while($row=mysqli_fetch_assoc($semuaPekerja)):  ?>
              <option value="<?= $row['pekerja_id']; ?>"><?= $row['nama_depan']." ".$row['nama_belakang']; ?></option>
            <?php endwhile; ?>
            
          </select>

        <label for="pelaksana2">Worker 2</label>
          <select id="pelaksana2" name="exe_pelaksana2" class="form-control browser-default custom-select-role" style="width: 100%;">
            <option value="0" selected>-- Worker 2 --</option>
             <?php while($row=mysqli_fetch_assoc($semuaPekerja1)):  ?>
              <option value="<?= $row['pekerja_id']; ?>"><?= $row['nama_depan']." ".$row['nama_belakang']; ?></option>
            <?php endwhile; ?>
          </select>

        <label for="pelaksana3">Worker 3</label>
          <select id="pelaksana3" name="exe_pelaksana3" class="form-control browser-default custom-select-role" style="width: 100%;">
            <option value="0" selected>-- Worker 3 --</option>
             <?php while($row=mysqli_fetch_assoc($semuaPekerja2)):  ?>
              <option value="<?= $row['pekerja_id']; ?>"><?= $row['nama_depan']." ".$row['nama_belakang']; ?></option>
            <?php endwhile; ?>
          </select>
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="submit" name="submit" class="btn btn-md btn-outline-default m-0 px-3 py-2 z-depth-0 waves-effect" id="button-addon2">SIMPAN</button>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>

</form>
