

  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="perbaikan-daftar.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">MAINTENANCE INFORMATION</h5>
            </div>
          </div>

          <div class="border border-primary mt-5 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Maintenance Information</strong></h4>
              <div class="form-row ">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Maintenance Name</strong></h6>
                      <p><?= $prev_namaperbaikan_a ?></p>

                      <h6 class="h6-responsive"><strong>Start Maintenance</strong></h6>
                      <p><?= $prev_startmtc_a ?></p>

                      <h6 class="h6-responsive"><strong>End Maintenance</strong></h6>
                      <p><?= $prev_endmtc_a ?></p>

                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Maintenance Type</strong></h6>
                      <p><?= $tmtc_jenisPerbaikan_a ?></p>

                      <h6 class="h6-responsive"><strong>Maintenance Description</strong></h6>
                      <p><?= $prev_keterangan_a ?></p>

                      <h6 class="h6-responsive"><strong>Maintenance Note</strong></h6>
                      <p><?= $prev_catatanMec_a ?></p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Priority</strong></h6>
                      <p><?= $pri_ket_priori_a ?></p>

                      <h6 class="h6-responsive"><strong>Maintenance Status</strong></h6>
                      <p class="red-text">On Progress</p>
                  </div>
              </div>
            </div>
          </div>
           <a href="perawatan-selesai.php?IdPerawatan=<?= $prev_id_a ?>&IdTipePerbaikan=<?= $prev_tipemtc_a ?>" type="submit" class="btn btn-success btn-lg waves-effect mt-4 net-mr float-right">DONE</a>
        </div>
      </div>

      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">

          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Maintenance History</strong></h4>

              <table id="dtHorizontalExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th class="th-sm">No.
                    </th>
                    <th class="th-sm">Mechanic Description
                    </th>
                    <th class="th-sm">Mechanic Note
                    </th>
                    <th class="th-sm">Maintenance Type
                    </th>
                    <th class="th-sm">Maintenance Start
                    </th>
                    <th class="th-sm">Maintenance End
                    </th>
                    <th class="th-sm">Interval
                    </th>
                    <th class="th-sm">Machine Status
                    </th>
                    <th class="th-sm">Worker
                    </th>
                    <th class="th-sm">Priority
                    </th>
                    <th class="th-sm">Maintenance Status
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php while($row=mysqli_fetch_assoc($semuaHistory)):  ?>
                  <tr>
                    <td><?= $row['prev_id'] ?></td>
                    <td><?= $row['prev_keterangan'] ?></td>
                    <td><?= $row['prev_catatanMec'] ?></td>
                    <td><?= $row['tmtc_jenisPerbaikan'] ?></td>
                    <td><?= $row['prev_startmtc'] ?></td>
                    <td><?= $row['prev_endmtc'] ?></td>
                    <td><?= $row['diff_startend'] ?> Hari</td>
                    <td>
                      <?php
                        if($row['prev_statusMesin'] == 1)
                        {
                            echo "Uptime"; }
                        else{
                            echo "Downtime";
                        }
                      ?>
                    </td>
                    <td><?= $row['nd_exe1'] ?> <?= $row['nb_exe1'] ?>, <?= $row['nd_exe2'] ?> <?= $row['nb_exe2'] ?>, <?= $row['nd_exe3'] ?> <?= $row['nb_exe3'] ?></td>
                    <td><?= $row['pri_ket_priori']?></td>
                    <td><?php
                        if($row['prev_resultStat'] == 0) {
                            echo "Belum Dikerjakan"; 
                        }if($row['prev_resultStat'] == 1) {
                            echo "Sedang Dikerjakan"; 
                        }if($row['prev_resultStat'] == 2) {
                            echo "Terjadi Kendala"; 
                        }if($row['prev_resultStat'] == 3) {
                            echo "Telah Selesai"; 
                        }
                      ?></td>
                  </tr>
                  <?php endwhile; ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>No.
                    </th>
                    <th>Mechanic Description
                    </th>
                    <th>Mechanic Note
                    </th>
                    <th>Maintenance Type
                    </th>
                    <th>Maintenance Start
                    </th>
                    <th>Maintenance End
                    </th>
                    <th>Interval
                    </th>
                    <th>Machine Status
                    </th>
                    <th>Worker
                    </th>
                    <th>Priority
                    </th>
                    <th>Maintenance Status
                    </th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>


          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Informasi Mesin</strong></h4>

              <div class="form-row">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Nama Mesin</strong></h6>
                      <p><?= $msn_nama_mesin_a ?></p>

                      <h6 class="h6-responsive"><strong>Jenis Mesin</strong></h6>
                      <p><?= $msn_jenis_mesin_a ?></p>

                      <h6 class="h6-responsive"><strong>Tipe Mesin</strong></h6>
                      <p><?= $msn_tipe_mesin_a ?></p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Deskripsi Mesin</strong></h6>
                      <p><?= $msn_deskripsi_mesin_a ?></p>
                  </div>
              </div>

              <div class="form-row mt-4">
                <div class="col">
                  <h6 class="h6-responsive"><strong>Tanggal Pembelian</strong></h6>
                  <p><?= $msn_tglbeli_mesin_a ?></p>

                  <h6 class="h6-responsive"><strong>Blok</strong></h6>
                  <p><?= $msn_blok_mesin_a ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Line</strong></h6>
                  <p><?= $msn_line_mesin_a ?></p>

                 <h6 class="h6-responsive"><strong>Nomor</strong></h6>
                  <p><?= $msn_nomor_mesin_a ?></p>
                </div>
              </div>
            </div>
          </div>

          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Informasi Pelaksana</strong></h4>

              <div class="form-row mt-4">
                <div class="col">
                  <h6 class="h6-responsive"><strong>Pelaksana 1</strong></h6>
                  <p><?= $perintah_nd_exe1_a ?> <?= $perintah_nb_exe1_a ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Pelaksana 2</strong></h6>
                  <p><?= $perintah_nd_exe2_a ?> <?= $perintah_nb_exe2_a ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Pelaksana 3</strong></h6>
                  <p><?= $perintah_nd_exe3_a ?> <?= $perintah_nb_exe3_a ?></p>
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Perintah Perbaikan</strong></h4>
              <div class="form-row ">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Nama Perbaikan</strong></h6>
                      <p><?= $prt_judul_a ?></p>

                      <h6 class="h6-responsive"><strong>Perintah Oleh</strong></h6>
                      <p><?= $perintah_namdep_hmd ?> <?= $perintah_nambel_hmd ?></p>

                      <h6 class="h6-responsive"><strong>Prioritas</strong></h6>
                      <p><?= $pri_ket_priori_a ?></p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Keterangan</strong></h6>
                      <p><?= $prt_keterangan_a ?></p>

                      <h6 class="h6-responsive"><strong>Catatan</strong></h6>
                      <p><?= $prt_catatan_a ?></p>
                  </div>
              </div>
            </div>
          </div> -->

      <!--Grid row-->
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->