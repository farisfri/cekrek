  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">

            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">MAINTENANCE LIST</h5>
            </div>
          </div>

          <div class="container-fluid mt-2">
            <table id="dtHorizontalExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="th-sm">No.
                  </th>
                  <th class="th-sm">Code
                  </th>
                  <th class="th-sm">Block
                  </th>
                  <th class="th-sm">Line
                  </th>
                  <th class="th-sm">Number
                  </th>
                  <th class="th-sm">Mechanic Description
                  </th>
                  <th class="th-sm">Mechanic Note
                  </th>
                  <th class="th-sm">Maintenance Type
                  </th>
                  <th class="th-sm">Start Maintenance
                  </th>
                  <th class="th-sm">End Maintenance
                  </th>
                  <th class="th-sm">Machine Status
                  </th>
                  <th class="th-sm">Worker
                  </th>
                  <th class="th-sm">MO Date
                  </th>
                  <th class="th-sm">Priority
                  </th>
                  <th class="th-sm">Maintenance Status
                  </th>
                  <th class="th-sm">Option
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php while($row=mysqli_fetch_assoc($semuaPerawatan)):  ?>
                <tr>
                  <td>1</td>
                  <td>XAN-0001</td>
                  <td>A</td>
                  <td>2</td>
                  <td>3</td>
                  <td>-</td>
                  <td>-</td>
                  <td>Gear Utama</td>
                  <td>5 Mei 2019 09.00</td>
                  <td>5 Mei 2019 11.00</td>
                  <td>Downtime</td>
                  <td>Faris Friansyah, Edo Kim, John Doe</td>
                  <td>4 Mei 2019</td>
                  <td>Sedang</td>
                  <td>Belum dikerjakan</td>
                  <td><a href="perawatan-info.php" class="btn btn-primary btn-sm btn-tabel" data-toggle="tooltip" title="Informasi Perawatan"><i class="fas fa-info-circle"></i></a>
                      <a href="perintah-hapus.php" class="btn btn-danger btn-sm btn-tabel" data-toggle="tooltip" title="Hapus Perintah"><i class="far fa-trash-alt"></i></a>
                  </td>
                </tr>
                <?php endwhile; ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No.
                  </th>
                  <th>Code
                  </th>
                  <th>Block
                  </th>
                  <th>Line
                  </th>
                  <th>Number
                  </th>
                  <th>Mechanic Description
                  </th>
                  <th>Mechanic Note
                  </th>
                  <th>Maintenance Type
                  </th>
                  <th>Start Maintenance
                  </th>
                  <th>End Maintenance
                  </th>
                  <th>Machine Status
                  </th>
                  <th>Worker
                  </th>
                  <th>MO Date
                  </th>
                  <th>Priority
                  </th>
                  <th>Maintenance Status
                  </th>
                  <th>Option
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->