  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="mesin-tambah.php" class="btn btn-success" style="margin-top: 8px;">
                Create Machine
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">MACHINE DATA</h5>
            </div>
          </div>

          <div class="container-fluid mt-2">
            <table id="dtHorizontalExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="th-sm">No.
                  </th>
                  <th class="th-sm">Code
                  </th>
                  <th class="th-sm">Machine Name
                  </th>
                  <th class="th-sm">Machine Model
                  </th>
                  <th class="th-sm">Machine Type
                  </th>
                  <th class="th-sm">Block
                  </th>
                  <th class="th-sm">Line
                  </th>
                  <th class="th-sm">Number
                  </th>
                  <th class="th-sm">Purchase Date
                  </th> 
                  <th class="th-sm">Option
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php while($row=mysqli_fetch_assoc($semuaMesin)):  ?>
                  <tr>
                    <td><?= $row['mesin_id']; ?></td>
                    <td><?= $row['mesin_code']; ?></td>
                    <td><?= $row['mesin_nama']; ?></td>
                    <td><?= $row['mesin_jenis']; ?></td>
                    <td><?= $row['mesin_tipe']; ?></td>
                    <td><?= $row['mesin_blok']; ?></td>
                    <td><?= $row['mesin_line']; ?></td>
                    <td><?= $row['mesin_nomor']; ?></td>
                    <td><?= $row['mesin_tglbeli']; ?></td>
                    <td><a href="mesin-gen.php?mesin_id=<?= $row['mesin_id']; ?>" class="btn btn-success btn-sm btn-tabel" data-toggle="tooltip" title="Generate QR Code"><i class="fas fa-qrcode"></i></a>
                        <a href="mesin-edit.php" class="btn btn-warning btn-sm btn-tabel" data-toggle="tooltip" title="Edit Data"><i class="far fa-edit"></i></i></a>
                    </td>
                  </tr>
                <?php endwhile; ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No.
                  </th>
                  <th>Code
                  </th>
                  <th>Machine Name
                  </th>
                  <th>Machine Model
                  </th>
                  <th>Machine Type
                  </th>
                  <th>Block
                  </th>
                  <th>Line
                  </th>
                  <th>Number
                  </th>
                  <th>Purchase Date
                  </th> 
                  <th>Option
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->