
  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->

      <div class="row">
      
        <div class="col-md-3">
          <a href="mesin-detail.php?mesin_idDetail=<?= $mesin_idDetail; ?>">
            <div class="card mb-4 wow fadeIn" style="width: 100%">
              <!--Card content-->
              <div class="card-body">
                <img class="mx-auto d-block" src="../assets/img/dash/conveyor.png" style="width: 70px; height: 70px;"><br>
                 <h5 class="h5-responsive text-center black-text">MACHINE DETAIL</h5>
              </div>
            </div>
          </a>
        </div>

        <div class="col-md-3">
          <a href="perbaikan-todo.php?mesin_idDetail=<?= $mesin_idDetail; ?>">
            <div class="card mb-4 wow fadeIn" style="width: 100%">
              <!--Card content-->
              <div class="card-body">
                <img class="mx-auto d-block" src="../assets/img/dash/wrench.png" style="width: 70px; height: 70px;"><br>
                 <h5 class="h5-responsive text-center black-text">REPAIR</h5>
              </div>
            </div>
          </a>
        </div>

        <div class="col-md-3">
          <a href="perawatan-todo.php?mesin_idDetail=<?= $mesin_idDetail; ?>">
            <div class="card mb-4 wow fadeIn" style="width: 100%">
              <!--Card content-->
              <div class="card-body">
                <img class="mx-auto d-block" src="../assets/img/dash/monitoring.png" style="width: 70px; height: 70px;"><br>
                 <h5 class="h5-responsive text-center black-text">MAINTENANCE</h5>
              </div>
            </div>
          </a>
        </div>

      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->