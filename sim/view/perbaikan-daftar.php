  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">

            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">Repair List</h5>
            </div>
          </div>

          <div class="container-fluid mt-2">
            <table id="dtHorizontalExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="th-sm">No.
                  </th>
                  <th class="th-sm">Machine Code
                  </th>
                  <th class="th-sm">Block
                  </th>
                  <th class="th-sm">Line
                  </th>
                  <th class="th-sm">Number
                  </th>
                  <th class="th-sm">Order Number
                  </th>
                  <th class="th-sm">Repair Name
                  </th>
                  <th class="th-sm">Mechanic Description
                  </th>
                  <th class="th-sm">Mechanic Note
                  </th>
                  <th class="th-sm">Maintenance Type
                  </th>
                  <th class="th-sm">Repair Start
                  </th>
                  <th class="th-sm">Repair End
                  </th>
                  <th class="th-sm">Created by
                  </th>
                  <th class="th-sm">Worker
                  </th>
                  <th class="th-sm">Date Order Created
                  </th>
                  <th class="th-sm">Priority
                  </th>
                  <th class="th-sm">Repair Status
                  </th>
                  <th class="th-sm">Option
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php while($row=mysqli_fetch_assoc($semuaPerbaikan)):  ?>
                <tr>
                  <td><?= $row['mtc_id']; ?></td>
                  <td><?= $row['prt_kode_mesin']; ?></td>
                  <td><?= $row['prt_blok_mesin']; ?></td>
                  <td><?= $row['prt_line_mesin']; ?></td>
                  <td><?= $row['prt_nomor_mesin']; ?></td>
                  <td><?= $row['mtc_perintahId']; ?></td>
                  <td><?= $row['prt_judul'];?></td>
                  <td><?= $row['mtc_keterangan']; ?></td>
                  <td><?= $row['mtc_catatanMec']; ?></td>
                  <td><?= $row['mtc_tipemtc']; ?></td>
                  <td><?= $row['mtc_startmtc']; ?></td>
                  <td><?= $row['mtc_endmtc']; ?></td>
                  <td><?= $row['prt_namdep_hmd']; ?> <?= $row['prt_nambel_hmd']; ?></td>
                  <td><?= $row['nd_exe1'] ?> <?= $row['nb_exe1'] ?>, <?= $row['nd_exe2'] ?> <?= $row['nb_exe2'] ?>, <?= $row['nd_exe3'] ?> <?= $row['nb_exe3'] ?></td>
                  <td><?= $row['prt_crtdate'];?></td>
                  <td><?= $row['prt_ket_priori']; ?></td>
                  <td><?php
                        if($row['mtc_resultStat'] == 0) {
                            echo "Not done"; 
                        }if($row['mtc_resultStat'] == 1) {
                            echo "Doing"; 
                        }if($row['mtc_resultStat'] == 2) {
                            echo "Obstacle occurred"; 
                        }if($row['mtc_resultStat'] == 3) {
                            echo "Done"; 
                        }
                      ?></td>
                  <td><a href="perbaikan-info.php?IdPerbaikan=<?= $row['mtc_id']; ?>" class="btn btn-primary btn-sm btn-tabel" data-toggle="tooltip" title="Informasi Perbaikan"><i class="fas fa-info-circle"></i></a>
                      <a href="perintah-hapus.php" class="btn btn-danger btn-sm btn-tabel" data-toggle="tooltip" title="Hapus Perintah"><i class="far fa-trash-alt"></i></a>
                  </td>
                </tr>
                <?php endwhile; ?>
              </tbody>
              <tfoot>
                <tr>
                   <th>No.
                  </th>
                  <th>Machine Code
                  </th>
                  <th>Block
                  </th>
                  <th>Line
                  </th>
                  <th>Number
                  </th>
                  <th>Order Number
                  </th>
                  <th>Repair Name
                  </th>
                  <th>Mechanic Description
                  </th>
                  <th>Mechanic Note
                  </th>
                  <th>Maintenance Type
                  </th>
                  <th>Repair Start
                  </th>
                  <th>Repair End
                  </th>
                  <th>Created by
                  </th>
                  <th>Worker
                  </th>
                  <th>Date Order Created
                  </th>
                  <th>Priority
                  </th>
                  <th>Repair Status
                  </th>
                  <th>Option
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->