

  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->

      <form action="" method="post" enctype="multipart/form-data">
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="perbaikan-daftar.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">PROBLEM REPORT</h5>
            </div>
          </div>

          <div class="border border-primary mt-5 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Repair Information</strong></h4>
              <div class="form-row ">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Repair Name</strong></h6>
                      <p><?= $prt_judul_a ?></p>

                      <h6 class="h6-responsive"><strong>Repair Start</strong></h6>
                      <p><?= $mtc_startmtc_a ?></p>

                      <h6 class="h6-responsive"><strong>Repair End</strong></h6>
                      <p><?= $mtc_endmtc_a ?></p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Repair Type</strong></h6>
                      <div class="input-group">
                        <select id="custom-select-role" class="form-control browser-default custom-select custom-select-role" name="mtc_tipemtc" aria-label="Example select with button addon">
                          <option selected disabled="">-- Repair Type --</option>
                          <?php while($row=mysqli_fetch_assoc($tampilMtc)):  ?>
                          <option value="<?= $row['tmtc_id'] ?>"><?= $row['tmtc_jenis'] ?></option>
                          <?php endwhile; ?>
                        </select>
                        <div class="input-group-append">
                          <button class="btn btn-md btn-primary m-0 px-3 py-2 z-depth-0 waves-effect" type="button" data-toggle="modal" data-target="#tambahTipe"><i class="fas fa-plus-circle"></i></button>
                        </div>
                      </div>


                      <div class="form-group">
                        <label for="mtc_keterangan">Mechanic Description</label>
                        <textarea class="form-control" id="mtc_keterangan" name="mtc_keterangan" rows="6" style="height: 100%;"></textarea>
                      </div>

                      <div class="form-group">
                        <label for="mtc_catatanMec">Mechanic Note</label>
                        <textarea class="form-control" id="mtc_catatanMec" name="mtc_catatanMec" rows="6" style="height: 100%;"></textarea>
                      </div>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Repair Status</strong></h6>
                      <p class="orange-text"><<?php
                        if($mtc_resultStat_a == 0) {
                            echo "Not Done"; 
                        }if($mtc_resultStat_a == 1) {
                            echo "Doing"; 
                        }if($mtc_resultStat_a == 2) {
                            echo "Obstacle occurred"; 
                        }if($mtc_resultStat_a == 3) {
                            echo "Done"; 
                        }
                      ?></p>
                  </div>
              </div>
            </div>
          </div>
            <button type="submit" name="simpan" class="btn btn-success btn-lg waves-effect mt-4 net-mr float-right">SAVE</button>
        </div>
      </div>
      </form>

      <div class="card mb-4 wow fadeIn" style="width: 100%">
      <!--Card content-->
      <div class="card-body ">
        <div class="border border-primary mt-3 p-4 rounded mb-0">
          <div class="container-fluid">
            <h4 class="h4-responsive mb-4"><strong>Machine Information</strong></h4>

            <div class="form-row">
                <div class="col">
                    <h6 class="h6-responsive"><strong>Machine Name</strong></h6>
                    <p><?= $msn_nama_mesin_a ?></p>

                    <h6 class="h6-responsive"><strong>Machine Model</strong></h6>
                    <p><?= $msn_jenis_mesin_a ?></p>

                    <h6 class="h6-responsive"><strong>Machine Type</strong></h6>
                    <p><?= $msn_tipe_mesin_a ?></p>
                </div>
                <div class="col">
                    <h6 class="h6-responsive"><strong>Machine Description</strong></h6>
                    <p><?= $msn_deskripsi_mesin_a ?></p>
                </div>
            </div>

            <div class="form-row mt-4">
              <div class="col">
                <h6 class="h6-responsive"><strong>Purchase Date</strong></h6>
                <p><?= $msn_tglbeli_mesin_a ?></p>

                <h6 class="h6-responsive"><strong>Block</strong></h6>
                <p><?= $msn_blok_mesin_a ?></p>
              </div>
              <div class="col">
                <h6 class="h6-responsive"><strong>Line</strong></h6>
                <p><?= $msn_line_mesin_a ?></p>

               <h6 class="h6-responsive"><strong>Numberr</strong></h6>
                <p><?= $msn_nomor_mesin_a ?></p>
              </div>
            </div>
          </div>
        </div>

        <div class="border border-primary mt-3 p-4 rounded mb-0">
          <div class="container-fluid">
            <h4 class="h4-responsive mb-4"><strong>Worker Information</strong></h4>

            <div class="form-row mt-4">
              <div class="col">
                <h6 class="h6-responsive"><strong>Worker 1</strong></h6>
                <p><?= $perintah_nd_exe1_a ?> <?= $perintah_nb_exe1_a ?></p>
              </div>
              <div class="col">
                <h6 class="h6-responsive"><strong>Worker 2</strong></h6>
                <p><?= $perintah_nd_exe2_a ?> <?= $perintah_nb_exe2_a ?></p>
              </div>
              <div class="col">
                <h6 class="h6-responsive"><strong>Worker 3</strong></h6>
                <p><?= $perintah_nd_exe3_a ?> <?= $perintah_nb_exe3_a ?></p>
              </div>
            </div>
          </div>
        </div>

        <div class="border border-primary mt-3 p-4 rounded mb-0">
          <div class="container-fluid">
            <h4 class="h4-responsive mb-4"><strong>Repair Order</strong></h4>
            <div class="form-row ">
                <div class="col">
                    <h6 class="h6-responsive"><strong>Repair Name</strong></h6>
                    <p><?= $prt_judul_a ?></p>

                    <h6 class="h6-responsive"><strong>Created by</strong></h6>
                    <p><?= $perintah_namdep_hmd ?> <?= $perintah_nambel_hmd ?></p>

                    <h6 class="h6-responsive"><strong>Priority</strong></h6>
                    <p><?= $pri_ket_priori_a ?></p>
                </div>
                <div class="col">
                    <h6 class="h6-responsive"><strong>Description</strong></h6>
                    <p><?= $prt_keterangan_a ?></p>

                    <h6 class="h6-responsive"><strong>Note</strong></h6>
                    <p><?= $prt_catatan_a ?></p>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Grid row-->
  </div>
</main>
<!--Main layout-->


<form action="" method="post" enctype="multipart/form-data">
<div class="modal fade" id="tambahTipe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-success" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2">Maintenance Type</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="md-form mb-5">
          <i class="fas fa-wrench prefix grey-text"></i>
          <input type="text" id="tmtc_jenisform" name="tmtc_jenis" class="form-control validate">
          <label data-error="wrong" data-success="right" for="tmtc_jenisform">Maintenance Type</label>
        </div>

        <!--Textarea with icon prefix-->
        <div class="md-form">
          <i class="fas fa-pencil-alt prefix"></i>
          <textarea id="form10" class="md-textarea form-control" name="tmtc_keterangan" rows="3"></textarea>
          <label for="form10">Maintenance Type Description</label>
        </div>
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="submit" name="submit" class="btn btn-success waves-effect">SAVE <i class="fas fa-paper-plane-o ml-1"></i></button>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
</form>
