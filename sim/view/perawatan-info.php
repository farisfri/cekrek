

  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="perbaikan-daftar.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">MAINTENANCE INFORMATION</h5>
            </div>
          </div>

          <div class="border border-primary mt-5 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Maintenance Information</strong></h4>
              <div class="form-row ">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Maintenance Name</strong></h6>
                      <p>Perbaiki Mesin Packaging</p>

                      <h6 class="h6-responsive"><strong>Start Maintenance</strong></h6>
                      <p>-</p>

                      <h6 class="h6-responsive"><strong>End Maintenance</strong></h6>
                      <p>-</p>

                      <h6 class="h6-responsive"><strong>Time Progress (MTTR)</strong></h6>
                      <p>120 Menit</p>

                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Maintenance Type</strong></h6>
                      <p>Gear Utama</p>

                      <h6 class="h6-responsive"><strong>Mechanic Description</strong></h6>
                      <p>-</p>

                      <h6 class="h6-responsive"><strong>Mechanic Note</strong></h6>
                      <p>-</p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Priority</strong></h6>
                      <p>Sedang</p>

                      <h6 class="h6-responsive"><strong>Next Maintenance Recommendation (MTBF)</strong></h6>
                      <p>5 Mei 2019</p>

                      <h6 class="h6-responsive"><strong>Status Maintenance</strong></h6>
                      <p class="red-text">Not Done</p>
                  </div>
              </div>
            </div>
          </div>
           <a href="perawatan-mulai.php" type="submit" class="btn btn-success btn-lg waves-effect mt-4 net-mr float-right">START</a>
        </div>
      </div>

      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">

          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Maintenance History</strong></h4>

              <table id="dtHorizontalExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th class="th-sm">No.
                    </th>
                    <th class="th-sm">Mechanic Description
                    </th>
                    <th class="th-sm">Mechanic Note
                    </th>
                    <th class="th-sm">Maintenance Type
                    </th>
                    <th class="th-sm">Start Maintenance
                    </th>
                    <th class="th-sm">End Maintenance
                    </th>
                    <th class="th-sm">Maintenance Status
                    </th>
                    <th class="th-sm">Worker
                    </th>
                    <th class="th-sm">Priority
                    </th>
                    <th class="th-sm">Maintenance Status
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Gear Utama</td>
                    <td>5 Maret 2019 09.00</td>
                    <td>5 Maret 2019 11.00</td>
                    <td>Downtime</td>
                    <td>Faris Friansyah, Edo Kim, John Doe</td>
                    <td>Sedang</td>
                    <td>Selesai</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Gear Utama</td>
                    <td>5 Maret 2019 11.00</td>
                    <td>5 April 2019 09.00</td>
                    <td>Uptime</td>
                    <td>Faris Friansyah, Edo Kim, John Doe</td>
                    <td>Sedang</td>
                    <td>Selesai</td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Gear Utama</td>
                    <td>5 April 2019 09.00</td>
                    <td>5 April 2019 11.00</td>
                    <td>Downtime</td>
                    <td>Faris Friansyah, Edo Kim, John Doe</td>
                    <td>Sedang</td>
                    <td>Selesai</td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Gear Utama</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Uptime</td>
                    <td>Faris Friansyah, Edo Kim, John Doe</td>
                    <td>Sedang</td>
                    <td>Belum Dikerjakan</td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <th>No.
                    </th>
                    <th>Mechanic Description
                    </th>
                    <th>Mechanic Note
                    </th>
                    <th>Maintenance Type
                    </th>
                    <th>Start Maintenance
                    </th>
                    <th>End Maintenance
                    </th>
                    <th>Maintenance Status
                    </th>
                    <th>Worker
                    </th>
                    <th>Priority
                    </th>
                    <th>Maintenance Status
                    </th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>

          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Machine Information</strong></h4>

              <div class="form-row">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Machine Name</strong></h6>
                      <p>XAN-0001</p>

                      <h6 class="h6-responsive"><strong>Machine Model</strong></h6>
                      <p>Packaging</p>

                      <h6 class="h6-responsive"><strong>Machine Type</strong></h6>
                      <p>XANXA Packaging</p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Machine Description</strong></h6>
                      <p>Mesin Packaging</p>
                  </div>
              </div>

              <div class="form-row mt-4">
                <div class="col">
                  <h6 class="h6-responsive"><strong>Purchase Date</strong></h6>
                  <p>4 April 2019</p>

                  <h6 class="h6-responsive"><strong>Block</strong></h6>
                  <p>A</p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Line</strong></h6>
                  <p>2</p>

                 <h6 class="h6-responsive"><strong>Number</strong></h6>
                  <p>3</p>
                </div>
              </div>
            </div>
          </div>

          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Worker Information</strong></h4>

              <div class="form-row mt-4">
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 1</strong></h6>
                  <p>Faris Friansyah</p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 2</strong></h6>
                  <p>Edo Kim</p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 3</strong></h6>
                  <p>John Doe</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->