

  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->


      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Machine Information</strong></h4>

              <div class="form-row">
                  <div class="col-md-4 col-sm-12 col-12">
                      <h6 class="h6-responsive"><strong>Machine Name</strong></h6>
                      <p><?= $mesin_nama_a ?></p>

                      <h6 class="h6-responsive"><strong>Machine Model</strong></h6>
                      <p><?= $mesin_jenis_a ?></p>

                      <h6 class="h6-responsive"><strong>Machine Type</strong></h6>
                      <p><?= $mesin_tipe_a ?></p>
                  </div>
                  <div class="col-md-4 col-sm-12 col-12">

                      <h6 class="h6-responsive"><strong>Code</strong></h6>
                      <p><?= $mesin_code_a ?></p>
                      
                      <h6 class="h6-responsive"><strong>Machine Description</strong></h6>
                      <p><?= $mesin_deskripsi_a ?></p>
                  </div>
                  <div class="col-md-4 col-sm-12 col-12">
                      <img src="view/<?= $mesin_qrcode_a ?>" class="float-right">
                  </div>
              </div>

              <div class="form-row mt-4">
                <div class="col-md-4 col-sm-12 col-12">
                  

                  <h6 class="h6-responsive"><strong>Block</strong></h6>
                  <p><?= $mesin_blok_a ?></p>

                  <h6 class="h6-responsive"><strong>Purchase Date</strong></h6>
                  <p><?= $mesin_tglbeli_a ?></p>
                </div>
                <div class="col-md-4 col-sm-12 col-12">
                  <h6 class="h6-responsive"><strong>Line</strong></h6>
                  <p><?= $mesin_line_a ?></p>

                  <h6 class="h6-responsive"><strong>Machine Status</strong></h6>
                  <p><?= $mtc_statusMesin_a ?></p>
                </div>
                <div class="col-md-4 col-sm-12 col-12">
                 <h6 class="h6-responsive"><strong>Number</strong></h6>
                  <p><?= $mesin_nomor_a ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->
