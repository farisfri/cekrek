<!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="pekerja-data.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>
              <a href="" class="btn btn-success" style="margin-top: 8px;" data-toggle="modal" data-target="#tambahBiodata">
                ADD Curriculum Vitae
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">Curriculum Vitae</h5>
            </div>
          </div>

          <div class="container-fluid mt-2">
            <table id="dataPekerja" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="th-sm">No.
                  </th>
                  <th class="th-sm">First Name
                  </th>
                  <th class="th-sm">Last Name
                  </th>
                  <th class="th-sm">Date of Birth
                  </th>
                  <th class="th-sm">Phone Number
                  </th>
                  <th class="th-sm">Option
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php while($row=mysqli_fetch_assoc($biodata)):  ?>
                <tr>
                  <td><?= $row['bio_id']; ?></td>
                  <td><?= $row['nama_depan']; ?></td>
                  <td><?= $row['nama_belakang']; ?></td>
                  <td><?= $row['tgl_lahir']; ?></td>
                  <td><?= $row['no_hp']; ?></td>
                  <td><a href="pekerja-akun.php?bio_id=<?= $row['bio_id']; ?>" class="btn btn-success btn-sm btn-tabel" data-toggle="tooltip" title="Tambah Data Akun"><i class="fas fa-plus"></i></a>
                      <a href="" class="btn btn-warning btn-sm btn-tabel" data-toggle="tooltip" title="Edit Data"><i class="far fa-edit"></i></i></a>
                      <a href="" class="btn btn-danger btn-sm btn-tabel"><i class="far fa-trash-alt" data-toggle="tooltip" title="Hapus Data"></i></a>
                  </td>
                </tr>
                <?php endwhile; ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No.
                  </th>
                  <th>First Name
                  </th>
                  <th>Last Name
                  </th>
                  <th>Date of Birth
                  </th>
                  <th>Phone Number
                  </th>
                  <th>Option
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->

  <form action="" method="post" enctype="multipart/form-data">

  <div class="modal fade" id="tambahBiodata" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-success" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2">Add CV</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      
        <!--Body-->
        <div class="modal-body">
          
            <div class="md-form mb-5">
              <i class="fas fa-user prefix grey-text"></i>
              <input type="text" id="namadepan" class="form-control validate" name="nama_depan">
              <label data-error="wrong" data-success="right" for="namadepan">First Name</label>
            </div>

            <div class="md-form">
              <i class="fas fa-user prefix grey-text"></i>
              <input type="text" id="namabelakang" class="form-control validate" name="nama_belakang">
              <label data-error="wrong" data-success="right" for="namabelakang">Last Name</label>
            </div>

            <div class="md-form">
              <i class="fas fa-calendar prefix grey-text"></i>
              <input type="text" class="form-control datepicker" value="2012-11-17" name="tgl_lahir">
              <label data-error="wrong" data-success="right" for="form2">Date of Birth</label>
            </div>

            <div class="md-form">
              <i class="fas fa-phone prefix grey-text"></i>
              <input type="number" id="form2" class="form-control validate" name="no_hp">
              <label data-error="wrong" data-success="right" for="form2">Phone Number</label>
            </div>
          </div>

          <!--Footer-->
          <div class="modal-footer justify-content-center">
            <button type="submit" name="submit" class="btn btn-success waves-effect">SAVE<i class="fas fa-paper-plane-o ml-1"></i></button>
          </div>
       
        
    </div>
    <!--/.Content-->
  </div>
</div>

 </form>
