
  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->

      <div class="row">
        
        <?php while($row=mysqli_fetch_assoc($notif_perbaikan)):  ?>

          <?php if($row['mtc_resultStat'] == 0){ ?>
          <div class="col-md-4">
            <a href="perbaikan-review.php?IdPerbaikan=<?= $row['mtc_id'] ?>">
              <div class="card mb-4 wow fadeIn" style="width: 100%">
                <!--Card content-->
                <div class="card-body">
                  <img class="mx-auto d-block" src="../assets/img/dash/conveyor.png" style="width: 70px; height: 70px;"><br>
                   <h5 class="h6-responsive text-center black-text"><strong><?= $row['msn_nama_mesin'] ?></strong></h5>
                   <h6 class="h6-responsive text-center black-text">Perintah Tanggal<br> <strong style="margin-top: 5px;"><?= $row['prt_crtdate'] ?></strong></h6>
                   <h6 class="h6-responsive text-center black-text">Prioritas <strong><?= $row['pri_ket_priori'] ?></strong></h6>
                </div>
              </div>
            </a>
          </div>

          <?php } else if($row['mtc_resultStat'] == 1) { ?>

          <div class="col-md-4">
            <a href="perbaikan-mulai.php?IdPerbaikan=<?= $row['mtc_id'] ?>">
              <div class="card mb-4 wow fadeIn" style="width: 100%">
                <!--Card content-->
                <div class="card-body">
                  <img class="mx-auto d-block" src="../assets/img/dash/conveyor.png" style="width: 70px; height: 70px;"><br>
                   <h5 class="h6-responsive text-center black-text"><strong><?= $row['msn_nama_mesin'] ?></strong></h5>
                   <h6 class="h6-responsive text-center black-text">Perintah Tanggal<br> <strong style="margin-top: 5px;"><?= $row['prt_crtdate'] ?></strong></h6>
                   <h6 class="h6-responsive text-center black-text">Prioritas <strong><?= $row['pri_ket_priori'] ?></strong></h6>
                </div>
              </div>
            </a>
          </div>

          <?php } else if($row['mtc_resultStat'] == 2) { ?>
            <div class="col-md-4">
              <a href="perbaikan-review.php?IdPerbaikan=<?= $row['mtc_id'] ?>">
                <div class="card mb-4 wow fadeIn" style="width: 100%">
                  <!--Card content-->
                  <div class="card-body">
                    <img class="mx-auto d-block" src="../assets/img/dash/conveyor.png" style="width: 70px; height: 70px;"><br>
                     <h5 class="h6-responsive text-center black-text"><strong><?= $row['msn_nama_mesin'] ?></strong></h5>
                     <h6 class="h6-responsive text-center black-text">Perintah Tanggal<br> <strong style="margin-top: 5px;"><?= $row['prt_crtdate'] ?></strong></h6>
                     <h6 class="h6-responsive text-center black-text">Prioritas <strong><?= $row['pri_ket_priori'] ?></strong></h6>
                  </div>
                </div>
              </a>
            </div>
          <?php } else if($row['mtc_resultStat'] == 3) { ?>
            <div class="col-md-4">
              <a href="perbaikan-berkala.php?IdPerbaikan=<?= $row['mtc_id'] ?>" disabled>
                <div class="card mb-4 wow fadeIn" style="width: 100%">
                  <!--Card content-->
                  <div class="card-body">
                    <img class="mx-auto d-block" src="../assets/img/dash/conveyor.png" style="width: 70px; height: 70px;"><br>
                     <h5 class="h6-responsive text-center black-text"><strong><?= $row['msn_nama_mesin'] ?></strong></h5>
                     <h6 class="h6-responsive text-center black-text">Perintah Tanggal<br> <strong style="margin-top: 5px;"><?= $row['prt_crtdate'] ?></strong></h6>
                     <h6 class="h6-responsive text-center black-text"><strong>Telah Selesai</strong></h6>
                  </div>
                </div>
              </a>
            </div>

          <?php } ?>

        <?php endwhile; ?>


      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->