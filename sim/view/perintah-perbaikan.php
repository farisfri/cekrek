

  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <form action="" method="post" enctype="multipart/form-data">
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="mesin-data.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">REPAIR ORDER</h5>
            </div>
          </div>


          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Repair Order</strong></h4>

              <label for="perintah_judul">Repair Name</label>
              <input type="text" id="perintah_judul" class="form-control validate" name="perintah_judul">

              <div class="form-row ">
                  <div class="col-md-6 col-sm-12 col-12">
                      <div class="form-group">
                        <label for="perintah_keterangan">Description</label>
                        <textarea class="form-control" id="perintah_keterangan" name="perintah_keterangan" rows="6" style="height: 100%;"></textarea>
                      </div>
                  </div>
                  <div class="col-md-6 col-sm-12 col-12">
                      <div class="form-group">
                        <label for="perintah_catatan">Note</label>
                        <textarea class="form-control" id="perintah_catatan" name="perintah_catatan" rows="6" style="height: 100%;"></textarea>
                      </div>
                  </div>
              </div>

              <label for="perintah_prioritas">Priority</label>
              <select id="perintah_prioritas" name="perintah_prioritas" class="form-control browser-default custom-select-role" style="width: 100%;">
                <option selected disabled="disabled">-- Choose priority --</option>
                <?php while($row=mysqli_fetch_assoc($semuaPrioritas)):  ?>
                  <option value="<?= $row['prio_id']; ?>"><?= $row['prio_keterangan']; ?></option>
                <?php endwhile; ?>
              </select>
            </div>
          </div>
          <?php echo $error; ?>
          <button type="submit" name="submit" class="btn btn-success waves-effect mt-4 net-mr float-right waves-effect" id="button-addon2">SAVE</button>
        </div>
      </form>
      </div>

      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Worker Information</strong></h4>

              <div class="form-row mt-4">
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 1</strong></h6>
                  <p><?= $nd_exe1; ?> <?= $nb_exe1; ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 2</strong></h6>
                  <p><?= $nd_exe2; ?> <?= $nb_exe2; ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 3</strong></h6>
                  <p><?= $nd_exe3; ?> <?= $nb_exe3; ?></p>
                </div>
              </div>
            </div>
          </div>

          <div class="border border-primary mt-5 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Machine Information</strong></h4>

              <div class="form-row">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Machine Name</strong></h6>
                      <p><?= $mesin_nama; ?></p>

                      <h6 class="h6-responsive"><strong>Model Machine</strong></h6>
                      <p><?= $mesin_jenis; ?></p>

                      <h6 class="h6-responsive"><strong>Machine Type</strong></h6>
                      <p><?= $mesin_tipe; ?></p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Machine Description</strong></h6>
                      <p><?= $mesin_deskripsi; ?></p>
                  </div>
              </div>

              <div class="form-row mt-4">
                <div class="col">
                  <h6 class="h6-responsive"><strong>Purchase Date</strong></h6>
                  <p><?= $mesin_tglbeli; ?></p>

                  <h6 class="h6-responsive"><strong>Block</strong></h6>
                  <p><?= $mesin_blok; ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Line</strong></h6>
                  <p><?= $mesin_line; ?></p>

                 <h6 class="h6-responsive"><strong>Number</strong></h6>
                  <p><?= $mesin_nomor; ?></p>
                </div>
              </div>
            </div>
          </div>

          
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->