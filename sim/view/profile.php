
  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5" style="padding-top: 20px; padding-bottom: 20px; ">
      <div class="card" style="width: 100%">
        <!-- Card content -->
          <div class="card-header">
            <a href="edit-profile.php" style="float: right;">
              <button type="button" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i> Edit</button>
            </a>
          </div>
          <div class="card-body" >
            <div class="row col-md-12">
              <div class="col-md-2">
                 <img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-8.jpg" alt="avatar" class="avatar rounded-circle d-flex align-self-center mr-2 z-depth-1">
              </div>
              <div class="col-md-10">
                <table width="100%">
                  <tbody>
                    <tr>
                      <td width="10%">
                        First Name
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%">
                        Joe
                      </td>
                      <td width="10%">
                        Department
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%"> 
                        Department 1
                      </td>
                    </tr>
                    <tr>
                      <td width="10%">
                        Last Name
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%">
                        Taslim
                      </td>
                      <td width="10%">
                        Area
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%"> 
                        Area 1
                      </td>
                    </tr>
                    <tr>
                      <td width="10%">
                        Gender
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%">
                        Male
                      </td>
                      <td width="10%">
                        Role
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%"> 
                        Manager
                      </td>
                    </tr>
                    <tr>
                      <td width="10%">
                        Username
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%">
                        JoeTaslim
                      </td>
                      <td width="10%">
                        Expertise
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%"> 
                        Medium
                      </td>
                    </tr>
                    <tr>
                      <td width="10%">
                        Email
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%">
                        joetaslim12@gmail.com
                      </td>
                      <td width="10%">
                      </td>
                      <td width="1%">
                      </td>
                      <td width="30%">
                      </td>
                    </tr>
                    <tr>
                      <td width="10%">
                        Phone
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%">
                        08564846152156
                      </td>
                      <td width="10%">
                      </td>
                      <td width="1%">
                      </td>
                      <td width="30%">
                      </td>
                    </tr>
                    <tr>
                      <td width="10%">
                        Password
                      </td>
                      <td width="1%">
                        :
                      </td>
                      <td width="30%">
                        ****************
                      </td>
                      <td width="10%">
                      </td>
                      <td width="1%">
                      </td>
                      <td width="30%">
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>
  </main>
  <!--Main layout-->