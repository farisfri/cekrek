
  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->

      <form action="" method="post" enctype="multipart/form-data">
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body">
          <div class="row">
            <div class="col-md-10">
              <select id="custom-select-role" class="form-control browser-default custom-select-role" style="width: 100%;" name="filter_tipe_perbaikan">
                <option selected>-- Choose Maintenance Type --</option>
                <?php while($row=mysqli_fetch_assoc($tampilMtc)):  ?>
                <option value="<?= $row['tmtc_id'] ?>"><?= $row['tmtc_jenis'] ?></option>
                <?php endwhile; ?>
              </select>
            </div>
            <div class="col-md-2">
              <button class="btn btn-primary btn-block" type="submit" name="submit" style="">FILTER</button>
            </div>
          </div>
        </div>
      </div>
      </form>

      <div class="row">
        
      <?php 

      if(!empty($filter_perawatan->num_rows)){
      while($row=mysqli_fetch_assoc($filter_perawatan)):  ?>
        <div class="col-md-4">
            <a href="perawatan-review.php?IdPerawatan=<?= $mesin_idDetail ?>&IdTipePerbaikan=<?= $filter_tipe_perbaikan ?>">
              <div class="card mb-4 wow fadeIn" style="width: 100%">
                <!--Card content-->
                <div class="card-body">
                  <img class="mx-auto d-block" src="../assets/img/dash/conveyor.png" style="width: 70px; height: 70px;"><br>
                   <h5 class="h6-responsive text-center black-text"><strong><?= $row['msn_nama_mesin'] ?></strong></h5>
                   <h6 class="h6-responsive text-center black-text">Damage Type <strong><?= $row['tmtc_jenisPerbaikan'] ?></strong></h6>
                </div>
              </div>
            </a>
          </div>
        <!--Grid row-->
      <?php endwhile; 
      }else{
         echo "";
      }
        
      ?>


      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->