  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="perintah-scan.php" class="btn btn-success" style="margin-top: 8px;">
                Add Order
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">List Order</h5>
            </div>
          </div>

          <div class="container-fluid mt-2">
            <table id="dtHorizontalExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="th-sm">No.
                  </th>
                  <th class="th-sm">Machine Code
                  </th>
                  <th class="th-sm">Block
                  </th>
                  <th class="th-sm">Line
                  </th>
                  <th class="th-sm">Number
                  </th>
                  <th class="th-sm">Repair name
                  </th>
                  <th class="th-sm">Description
                  </th>
                  <th class="th-sm">Note
                  </th>
                  <th class="th-sm">Create
                  </th>
                  <th class="th-sm">Worker 1
                  </th>
                  <th class="th-sm">Worker 2 
                  </th>
                  <th class="th-sm">Worker 3
                  </th>
                  <th class="th-sm">Date Created
                  </th>
                  <th class="th-sm">Priority
                  </th>
                  <th class="th-sm">Option
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php while($row=mysqli_fetch_assoc($semuaPerintah)):  ?>
                <tr>
                  <td><?= $row['perintah_id']; ?></td>
                  <td><?= $row['prt_kode_mesin']; ?></td>
                  <td><?= $row['prt_blok_mesin']; ?></td>
                  <td><?= $row['prt_line_mesin']; ?></td>
                  <td><?= $row['prt_nomor_mesin']; ?></td>
                  <td><?= $row['perintah_judul']; ?></td>
                  <td><?= $row['perintah_keterangan']; ?></td>
                  <td><?= $row['perintah_catatan']; ?></td>
                  <td><?= $row['prt_namdep_hmd']; ?> <?= $row['prt_nambel_hmd']; ?></td>
                  <td><?= $row['nd_exe1']; ?> <?= $row['nb_exe1']; ?></td>
                  <td><?= $row['nd_exe2']; ?> <?= $row['nb_exe2']; ?></td>
                  <td><?= $row['nd_exe3']; ?> <?= $row['nb_exe3']; ?></td>
                  <td><?= $row['perintah_crtdate']; ?></td>
                  <td><?= $row['prt_ket_priori']; ?></td>
                  <td>
                      <a href="perintah-info.php?perintah_id_lihat=<?= $row['perintah_id']; ?>" class="btn btn-primary btn-sm btn-tabel" data-toggle="tooltip" title="View Order"><i class="fas fa-binoculars"></i></a>
                      <a href="perintah-hapus.php" class="btn btn-danger btn-sm btn-tabel" data-toggle="tooltip" title="Delete Order"><i class="far fa-trash-alt"></i></a>
                  </td>
                </tr>
                <?php endwhile; ?>
              </tbody>
              <tfoot>
                <tr>
                  <th >No.
                  </th>
                  <th>Machine Code
                  </th>
                  <th >Block
                  </th>
                  <th >Line
                  </th>
                  <th>Number
                  </th>
                  <th >Repair name
                  </th>
                  <th>Description
                  </th>
                  <th >Note
                  </th>
                  <th >Create
                  </th>
                  <th >Worker 1
                  </th>
                  <th >Worker 2 
                  </th>
                  <th >Worker 3
                  </th>
                  <th>Date Created
                  </th>
                  <th >Priority
                  </th>
                  <th >Option
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->