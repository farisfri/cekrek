

  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="perbaikan-daftar.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>

              <?php echo $error; ?>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">REPAIR INFORMATION</h5>
            </div>
          </div>

          <div class="border border-primary mt-5 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Repair Information</strong></h4>
              <div class="form-row ">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Repair Name</strong></h6>
                      <p><?= $prt_judul_a ?></p>

                      <h6 class="h6-responsive"><strong>Repair Start</strong></h6>
                      <p><?= $mtc_startmtc_a ?></p>

                      <h6 class="h6-responsive"><strong>Repair End</strong></h6>
                      <p><?= $mtc_endmtc_a ?></p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Maintenance Type</strong></h6>
                      <p><?= $mtc_tipemtc_a ?></p>

                      <h6 class="h6-responsive"><strong>Mechanic Description</strong></h6>
                      <p><?= $mtc_keterangan_a ?></p>

                      <h6 class="h6-responsive"><strong>Mechanic Note</strong></h6>
                      <p><?= $mtc_catatanMec_a ?></p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Repair Status</strong></h6>
                      <p class="red-text"><?php
                        if($mtc_resultStat_a == 0) {
                            echo "Not done"; 
                        }if($mtc_resultStat_a == 1) {
                            echo "Doing"; 
                        }if($mtc_resultStat_a == 2) {
                            echo "Obstacle occurred"; 
                        }if($mtc_resultStat_a == 3) {
                            echo "Done"; 
                        }
                      ?></p>
                  </div>
              </div>
            </div>
          </div>
          <form action="" method="post" enctype="multipart/form-data">
           <button type="submit" name="submit" class="btn btn-success btn-lg waves-effect mt-4 net-mr float-right">TO DO CARE <i class="fas fa-paper-plane-o ml-1"></i></button>
           </form>
        </div>
      </div>

      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Machine Information</strong></h4>

              <div class="form-row">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Machine Name</strong></h6>
                      <p><?= $msn_nama_mesin_a ?></p>

                      <h6 class="h6-responsive"><strong>Machine Model</strong></h6>
                      <p><?= $msn_jenis_mesin_a ?></p>

                      <h6 class="h6-responsive"><strong>Machine Type</strong></h6>
                      <p><?= $msn_tipe_mesin_a ?></p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Machine Description</strong></h6>
                      <p><?= $msn_deskripsi_mesin_a ?></p>
                  </div>
              </div>

              <div class="form-row mt-4">
                <div class="col">
                  <h6 class="h6-responsive"><strong>Purchase Date</strong></h6>
                  <p><?= $msn_tglbeli_mesin_a ?></p>

                  <h6 class="h6-responsive"><strong>Block</strong></h6>
                  <p><?= $msn_blok_mesin_a ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Line</strong></h6>
                  <p><?= $msn_line_mesin_a ?></p>

                 <h6 class="h6-responsive"><strong>Number</strong></h6>
                  <p><?= $msn_nomor_mesin_a ?></p>
                </div>
              </div>
            </div>
          </div>

          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Worker Information</strong></h4>

              <div class="form-row mt-4">
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 1</strong></h6>
                  <p><?= $perintah_nd_exe1_a ?> <?= $perintah_nb_exe1_a ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 2</strong></h6>
                  <p><?= $perintah_nd_exe2_a ?> <?= $perintah_nb_exe2_a ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 3</strong></h6>
                  <p><?= $perintah_nd_exe3_a ?> <?= $perintah_nb_exe3_a ?></p>
                </div>
              </div>
            </div>
          </div>

          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Repair Order</strong></h4>
              <div class="form-row ">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Repair Name</strong></h6>
                      <p><?= $prt_judul_a ?></p>

                      <h6 class="h6-responsive"><strong>Created by</strong></h6>
                      <p><?= $perintah_namdep_hmd ?> <?= $perintah_nambel_hmd ?></p>

                      <h6 class="h6-responsive"><strong>Priority</strong></h6>
                      <p><?= $pri_ket_priori_a ?></p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Description</strong></h6>
                      <p><?= $prt_keterangan_a ?></p>

                      <h6 class="h6-responsive"><strong>Note</strong></h6>
                      <p><?= $prt_catatan_a ?></p>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->
