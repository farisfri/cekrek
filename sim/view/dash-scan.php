  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body">

          <div id="error"><?= $error ?></div>

          <form action="" method="post" class="mt-3" >

          <div class="input-group mb-3">

            <input type="text" size="16" placeholder="Tracking Code" class="form-control qrcode-text" name="mesin_scanQR">
            <label class="qrcode-text-btn">
              <input type=file accept="image/*" capture=environment onclick="return showQRIntro();" value="return showQRIntro();" onchange="openQRCamera(this);" tabindex=-1>
            </label> 
            <div class="input-group-append">
              <button type="submit" name="submit" class="btn btn-md btn-outline-default m-0 px-3 py-2 z-depth-0 waves-effect" id="button-addon2">Search</button>
            </div>
          </div>

           </form>

        </div>
      </div>
      <!--Grid row-->

      <?php 

      if(!empty($pencarianQRMesin->num_rows)){
      while($row=mysqli_fetch_assoc($pencarianQRMesin)):  ?>
        <div class="card mb-4 wow fadeIn" style="width: 100%">
          <!--Card content-->
          <div class="card-body">

            <div class="row">
              <div class="col-md-6">
                <h5><?= $row['mesin_nama']; ?></h5>
                <p><?= $row['mesin_jenis']; ?></p>
                <p><?= $row['mesin_tipe']; ?></p>
              </div>
              <div class="col-md-6 text-right">
                <a href="mesin-preview.php?mesin_idDetail=<?= $row['mesin_id']; ?>" class="btn btn-success btn-sm btn-tabel"><i class="fas fa-arrow-right"></i></a>
              </div>
            </div>

          </div>
        </div>
        <!--Grid row-->
      <?php endwhile; 
      }else{
         echo "";
      }
        
      ?>

    </div>
  </main>
  <!--Main layout-->

  <script type="text/javascript">
    function openQRCamera(node) {
      var reader = new FileReader();
      reader.onload = function() {
        node.value = "";
        qrcode.callback = function(res) {
          if(res instanceof Error) {
            alert("No QR code found. Please make sure the QR code is within the camera's frame and try again.");
          } else {
            node.parentNode.previousElementSibling.value = res;
          }
        };
        qrcode.decode(reader.result);
      };
      reader.readAsDataURL(node.files[0]);
    }

    function showQRIntro() {
      return confirm("Use your camera to take a picture of a QR code.");
    }
  </script>