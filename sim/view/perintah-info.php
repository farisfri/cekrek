

  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="perbaikan-daftar.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">Repair Order Information</h5>
            </div>
          </div>

          <div class="border border-primary mt-5 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Repair Information</strong></h4>
              <div class="form-row ">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Repair Name</strong></h6>
                      <p><?= $perintah_judul_a ?></p>

                      <h6 class="h6-responsive"><strong>Order Date</strong></h6>
                      <p><?= $perintah_crtdate_a ?></p>

                      <h6 class="h6-responsive"><strong>Created by</strong></h6>
                      <p><?= $perintah_namdep_hmd ?> <?= $perintah_nambel_hmd ?></p>
                  </div>
                  <div class="col">


                      <h6 class="h6-responsive"><strong>Description</strong></h6>
                      <p><?= $perintah_judul_a ?></p>

                      <h6 class="h6-responsive"><strong>Note</strong></h6>
                      <p><?= $perintah_catatan_a ?></p>

                      <h6 class="h6-responsive"><strong>Repair status</strong></h6>
                      <p class="red-text">Not Yet</p>
                  </div>
              </div>
            </div>
          </div>
           <form action="" method="post" enctype="multipart/form-data">
           <button type="submit" name="submit" class="btn btn-success btn-lg waves-effect mt-4 net-mr float-right">To Do Maintenance <i class="fas fa-paper-plane-o ml-1"></i></button>
           </form>
        </div>
      </div>

      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Machine Information</strong></h4>

              <div class="form-row">
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Machine Name</strong></h6>
                      <p><?= $prt_nama_mesin_a ?></p>

                      <h6 class="h6-responsive"><strong>Machine Model</strong></h6>
                      <p><?= $prt_jenis_mesin_a ?></p>

                      <h6 class="h6-responsive"><strong>Machine Description</strong></h6>
                      <p><?= $prt_tipe_mesin_a ?></p>
                  </div>
                  <div class="col">
                      <h6 class="h6-responsive"><strong>Purchase Date</strong></h6>
                      <p><?= $prt_tglbeli_mesin_a ?></p>

                      <h6 class="h6-responsive"><strong>Machine Code</strong></h6>
                      <p><?= $prt_kode_mesin_a ?></p>

                      <h6 class="h6-responsive"><strong>Machine Description</strong></h6>
                      <p><?= $prt_deskripsi_mesin_a ?></p>
                  </div>
                  <div class="col">
                    <h6 class="h6-responsive"><strong>Block</strong></h6>
                    <p><?= $prt_blok_mesin_a ?></p>

                    <h6 class="h6-responsive"><strong>Line</strong></h6>
                    <p><?= $prt_line_mesin_a ?></p>

                   <h6 class="h6-responsive"><strong>Number</strong></h6>
                    <p><?= $prt_nomor_mesin_a ?></p>
                  </div>
              </div>

            </div>
          </div>

          <div class="border border-primary mt-3 p-4 rounded mb-0">
            <div class="container-fluid">
              <h4 class="h4-responsive mb-4"><strong>Worker Information</strong></h4>

              <div class="form-row mt-4">
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 1</strong></h6>
                  <p><?= $perintah_nd_exe1_a ?> <?= $perintah_nb_exe1_a ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 2</strong></h6>
                  <p><?= $perintah_nd_exe2_a ?> <?= $perintah_nb_exe2_a ?></p>
                </div>
                <div class="col">
                  <h6 class="h6-responsive"><strong>Worker 3</strong></h6>
                  <p><?= $perintah_nd_exe3_a ?> <?= $perintah_nb_exe3_a ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->