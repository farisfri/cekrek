

  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="mesin-data.php" class="btn btn-danger" style="margin-top: 8px;">
                Back
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">EDIT MACHINE INFORMATION</h5>
            </div>
          </div>

          <div class="container-fluid mt-5">
            <div class="form-row ">
                <div class="col">
                    <!-- First name -->
                    <label for="mesin_nama">Machine Name</label>
                    <input type="text" id="mesin_nama" class="form-control validate" name="mesin_nama">

                    <!-- First name -->
                    <label for="mesin_jenis">Machine Model</label>
                    <input type="text" id="mesin_jenis" class="form-control validate" name="mesin_jenis">

                    <!-- First name -->
                    <label for="mesin_tipe">Machine Type</label>
                    <input type="text" id="mesin_tipe" class="form-control validate" name="mesin_tipe">
                </div>
                <div class="col">
                    <div class="form-group">
                      <label for="mesin_deskripsi">Machine Description</label>
                      <textarea class="form-control" id="mesin_deskripsi" name="mesin_deskripsi" rows="6" style="height: 100%;"></textarea>
                    </div>
                </div>
            </div>

            <div class="form-row mt-4">
              <div class="col">
                <label for="mesin_tglbeli">Purchase Date</label>
                <input type="text" class="form-control datepicker" value="02-16-2012" id="mesin_tglbeli" name="mesin_tglbeli">

                <label for="mesin_blok">Block</label>
                <input type="text" id="mesin_blok" class="form-control validate" name="mesin_blok">
              </div>
              <div class="col">
                <label for="mesin_line">Line</label>
                <input type="text" id="mesin_line" class="form-control validate" name="mesin_line">

                <label for="mesin_nomor">Number</label>
                <input type="text" id="mesin_nomor" class="form-control validate" name="mesin_nomor">
              </div>
            </div>
            <a type="button" class="btn btn-success waves-effect mt-4 net-mr float-right">Save <i class="fas fa-paper-plane-o ml-1"></i></a>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->