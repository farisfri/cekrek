  <!--Main layout-->
  <main class="pt-5 mx-lg-3">
    <div class="container-fluid mt-5">
      <!-- Heading -->
      <div class="card mb-4 wow fadeIn" style="width: 100%">
        <!--Card content-->
        <div class="card-body ">
          <div class="clearfix">
            <div class="row role-header float-left">
              <a href="pekerja-bio.php" class="btn btn-success" style="margin-top: 8px;">
                Add Engineer
              </a>
            </div>
            <div class="row role-header float-right">
              <h5 class="h5-responsive text-right">ENGINEER DATA</h5>
            </div>
          </div>

          <div class="container-fluid mt-2">
            <table id="dtHorizontalExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="th-sm">No.
                  </th>
                  <th class="th-sm">User Code
                  </th>
                  <th class="th-sm">Username
                  </th>
                  <th class="th-sm">First Name
                  </th>
                  <th class="th-sm">Last Name
                  </th>
                  <th class="th-sm">Email
                  </th>
                  <th class="th-sm">Phone Number
                  </th>
                  <th class="th-sm">Job Title
                  </th>
                  <th class="th-sm">Role
                  </th>
                  <th class="th-sm">Option
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php while($row=mysqli_fetch_assoc($semuaPekerja)):  ?>
                <tr>
                  <td><?= $row['pekerja_id']; ?></td>
                  <td><?= $row['krywn_code']; ?></td>
                  <td><?= $row['krywn_username']; ?></td>
                  <td><?= $row['nama_depan']; ?></td>
                  <td><?= $row['nama_belakang']; ?></td>
                  <td><?= $row['krywn_email']; ?></td>
                  <td><?= $row['no_hp']; ?></td>
                  <td><?= $row['krywn_jabatan']; ?></td>
                  <td><?= $row['krywn_role']; ?></td>
                  <td><a href="pekerja-gen.php?pekerja_id=<?= $row['pekerja_id']; ?>" class="btn btn-success btn-sm btn-tabel" data-toggle="tooltip" title="Generate QR Code"><i class="fas fa-qrcode"></i></a>
                      <a href="pekerja-edit.php" class="btn btn-warning btn-sm btn-tabel" data-toggle="tooltip" title="Edit Data"><i class="far fa-edit"></i></i></a>
                  </td>
                </tr>
                <?php endwhile; ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No.
                  </th>
                  <th>User Code
                  </th>
                  <th>Username
                  </th>
                  <th>First Name
                  </th>
                  <th>Last Name
                  </th>
                  <th>Email
                  </th>
                  <th>Phone Number
                  </th>
                  <th>Job Title
                  </th>
                  <th>Role
                  </th>
                  <th>Option
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <!--Grid row-->
    </div>
  </main>
  <!--Main layout-->