<?php

require_once "core/init.php";

$IdPerawatan = $_GET['IdPerawatan'];
$IdTipePerbaikan = $_GET['IdTipePerbaikan'];

if(isset($_GET['IdPerawatan'])){
    $perawatan= tampilkan_per_perawatanDetail($IdPerawatan, $IdTipePerbaikan);
    while($row=mysqli_fetch_assoc($perawatan)){
        		      $prev_id_a = $row['prev_id'];
                  $prev_mesinId_a = $row['prev_mesinId'];
                  $prev_startmtc_a = $row['prev_startmtc'];
                  $prev_endmtc_a = $row['prev_endmtc'];
                  $diff_startend_a = $row['diff_startend'];
                  $diff_before_a = $row['diff_before'];
                  $prev_namaperbaikan_a = $row['prev_namaperbaikan'];
                  $prev_keterangan_a = $row['prev_keterangan'];
                  $prev_catatanMec_a = $row['prev_catatanMec'];
                  $prev_tipemtc_a = $row['prev_tipemtc'];
                  $prev_statusMesin_a= $row['prev_statusMesin'];
                  $prev_pelaksana_a= $row['prev_pelaksana'];
                  $prev_resultStat_a= $row['prev_resultStat'];
                  $prev_priority_a= $row['prev_priority'];
                  $msn_Id_mesin_a= $row['msn_Id_mesin'];
                  $msn_nama_mesin_a= $row['msn_nama_mesin'];
                  $msn_jenis_mesin_a= $row['msn_jenis_mesin'];
                  $msn_tipe_mesin_a= $row['msn_tipe_mesin'];
                  $msn_deskripsi_mesin_a= $row['msn_deskripsi_mesin'];
                  $msn_tglbeli_mesin_a= $row['msn_tglbeli_mesin'];
                  $msn_kode_mesin_a= $row['msn_kode_mesin'];
                  $msn_blok_mesin_a= $row['msn_blok_mesin'];
                  $msn_line_mesin_a= $row['msn_line_mesin'];
                  $msn_nomor_mesin_a=$row['msn_nomor_mesin'];
                  $perintah_nd_exe1_a= $row['nd_exe1'];
                  $perintah_nb_exe1_a=$row['nb_exe1'];
                  $perintah_nd_exe2_a= $row['nd_exe2'];
                  $perintah_nb_exe2_a=$row['nb_exe2'];
                  $perintah_nd_exe3_a= $row['nd_exe3'];
                  $perintah_nb_exe3_a=$row['nb_exe3'];
                  $tmtc_jenisPerbaikan_a= $row['tmtc_jenisPerbaikan'];
                  $pri_ket_priori_a=$row['pri_ket_priori'];
    }
}

$semuaHistory = tampilHistoryDetail($IdPerawatan, $IdTipePerbaikan, $prev_mesinId_a);

require_once "layout/head.php";
require_once "view/perawatan-mulai.php";
require_once "layout/footer.php";

?>
