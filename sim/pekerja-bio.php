<?php
require_once "core/init.php";

 $error ='';
 if(isset($_POST['submit'])){
     $nama_depan = $_POST['nama_depan'];
     $nama_belakang = $_POST['nama_belakang'];
     $tgl_lahir = $_POST['tgl_lahir'];
     $no_hp = $_POST['no_hp'];

     if(!empty(trim($nama_depan)) && !empty(trim($nama_belakang))){
         if(register_biodata($nama_depan, $nama_belakang, $tgl_lahir, $no_hp)){
             header('location: pekerja-bio.php');
         }else{
             $error='ada masalah saat menambah data';
         }

     }else{
       $error = 'judul dan konten wajib diisi';
	  }
   }

$biodata = tampil_biodata();

require_once "layout/head.php";
require_once "view/pekerja-bio.php";
require_once "layout/footer.php";

?>