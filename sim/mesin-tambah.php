<?php

require_once "core/init.php";
$error ='';
if(isset($_POST['submit'])){
    $mesin_nama = $_POST['mesin_nama'];
    $mesin_jenis = $_POST['mesin_jenis'];
    $mesin_tipe = $_POST['mesin_tipe'];
    $mesin_deskripsi = $_POST['mesin_deskripsi'];
    $mesin_tglbeli = $_POST['mesin_tglbeli'];
    $mesin_blok = $_POST['mesin_blok'];
    $mesin_line = $_POST['mesin_line'];
    $mesin_nomor = $_POST['mesin_nomor'];


    if(!empty(trim($mesin_nama)) && !empty(trim($mesin_jenis))){
        if(register_mesin($mesin_nama, $mesin_jenis, $mesin_tipe, $mesin_deskripsi, $mesin_tglbeli, $mesin_blok, $mesin_line, $mesin_nomor)){
            header('location: mesin-data.php');
        }else{
            $error='ada masalah saat menambah data';
        }

    }else{
       $error = 'judul dan konten wajib diisi';
	}
}

require_once "layout/head.php";
require_once "view/mesin-tambah.php";
require_once "layout/footer.php";

?>