<?php

require_once "core/init.php";

$biodata_id = $_GET['bio_id'];

$error ='';
if(isset($_POST['submit'])){
    $krywn_username = $_POST['krywn_username'];
    $krywn_email = $_POST['krywn_email'];
    $krywn_password = $_POST['krywn_password'];
    $krywn_jabatan = $_POST['krywn_jabatan'];
    $krywn_role = $_POST['krywn_role'];


    if(!empty(trim($krywn_username)) && !empty(trim($krywn_password))){
        if(register_akun($biodata_id, $krywn_username, $krywn_email, $krywn_password, $krywn_jabatan, $krywn_role)){
            header('location: pekerja-data.php');
        }else{
            $error='ada masalah saat menambah data';
        }

    }else{
       $error = 'judul dan konten wajib diisi';
	}
}

require_once "layout/head.php";
require_once "view/pekerja-akun.php";
require_once "layout/footer.php";

?>