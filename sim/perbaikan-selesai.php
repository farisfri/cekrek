<?php

require_once "core/init.php";

$IdPerbaikan = $_GET['IdPerbaikan'];

if(isset($_GET['IdPerbaikan'])){
    $perbaikan = tampilkan_per_perbaikan($IdPerbaikan);
    while($row=mysqli_fetch_assoc($perbaikan)){

        		  $mtc_id_a = $row['mtc_id'];
              $mtc_startmtc_a = $row['mtc_startmtc'];
              $mtc_endmtc_a = $row['mtc_endmtc'];
              $mtc_keterangan_a = $row['mtc_keterangan'];
              $mtc_catatanMec_a = $row['mtc_catatanMec'];
              $mtc_tipemtc_a = $row['mtc_tipemtc'];
              $mtc_statusMesin_a = $row['mtc_statusMesin'];
              $mtc_crtuser_a = $row['mtc_crtuser'];
              $mtc_resultStat_a = $row['mtc_resultStat'];
              $msn_kode_mesin_a= $row['msn_kode_mesin'];
              $msn_Id_mesin_a= $row['msn_Id_mesin'];
              $msn_nama_mesin_a= $row['msn_nama_mesin'];
              $msn_jenis_mesin_a= $row['msn_jenis_mesin'];
              $msn_tipe_mesin_a= $row['msn_tipe_mesin'];
              $msn_deskripsi_mesin_a= $row['msn_deskripsi_mesin'];
              $msn_tglbeli_mesin_a= $row['msn_tglbeli_mesin'];
              $msn_blok_mesin_a= $row['msn_blok_mesin'];
              $msn_line_mesin_a= $row['msn_line_mesin'];
              $msn_nomor_mesin_a= $row['msn_nomor_mesin'];
              $prt_judul_a= $row['prt_judul'];
              $prt_keterangan_a= $row['prt_keterangan'];
              $prt_catatan_a= $row['prt_catatan'];
              $perintah_namdep_hmd= $row['prt_namdep_hmd'];
              $perintah_nambel_hmd=$row['prt_nambel_hmd'];
              $perintah_nd_exe1_a= $row['nd_exe1'];
              $perintah_nb_exe1_a=$row['nb_exe1'];
              $perintah_nd_exe2_a= $row['nd_exe2'];
              $perintah_nb_exe2_a=$row['nb_exe2'];
              $perintah_nd_exe3_a= $row['nd_exe3'];
              $perintah_nb_exe3_a=$row['nb_exe3'];
              $perintah_crtdate_a= $row['prt_crtdate'];
              $pri_ket_priori_a= $row['pri_ket_priori'];
    }
}

 $error ='';
 if(isset($_POST['submit'])){
     $tmtc_jenis = $_POST['tmtc_jenis'];
     $tmtc_keterangan = $_POST['tmtc_keterangan'];

     if(!empty(trim($tmtc_jenis)) && !empty(trim($tmtc_keterangan))){
         if(register_tipemtc($tmtc_jenis, $tmtc_keterangan)){
             header('location: perbaikan-kendala.php'."?IdPerbaikan=".$mtc_id_a);
         }else{
             $error='ada masalah saat menambah data';
         }

     }else{
       $error = 'judul dan konten wajib diisi';
	  }
   }

$tampilMtc = tampilMtc();

 $error ='';
 if(isset($_POST['simpan'])){
     $mtc_tipemtc = $_POST['mtc_tipemtc'];
     $mtc_keterangan = $_POST['mtc_keterangan'];
     $mtc_catatanMec = $_POST['mtc_catatanMec'];

     if(!empty(trim($mtc_tipemtc))){
         if(selesai_perbaikan($mtc_tipemtc, $mtc_keterangan, $mtc_catatanMec, $mtc_id_a)){
             header('location: mesin-preview.php'."?mesin_idDetail=".$msn_Id_mesin_a);
         }else{
             $error='ada masalah saat menambah data';
         }

     }else{
       $error = 'judul dan konten wajib diisi';
	  }
}

require_once "layout/head.php";
require_once "view/perbaikan-selesai.php";
require_once "layout/footer.php";

?>
