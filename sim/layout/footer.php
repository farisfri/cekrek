  <!-- Full Height Modal Right -->
  <div class="modal fade right" id="notification" tabindex="-1" role="dialog" aria-labelledby="notif" aria-hidden="true">

    <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
    <div class="modal-dialog modal-full-height modal-right" role="document">


      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title w-100" id="notif">Notification</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container-fluid mb-3" >

              <div class="row mb-3">
                <div class="card" style="width: 100%; padding: 15px;">
                  <div class="card-body">
                    <p>New message from <b>Khuril</b></p>
                  </div>
                  <div class="card-footer text-center">
                    <span>27 September 2018 17.04</span>
                  </div>
                </div>
              </div>

              <div class="row mb-3">
                <div class="card" style="width: 100%; padding: 15px;">
                  <div class="card-body">
                    <p><b>Reza Rahardian</b> create a new course</p>
                  </div>
                  <div class="card-footer text-center">
                    <span>27 September 2018 17.04</span>
                  </div>
                </div>
              </div>

              <div class="row mb-3">
                <div class="card" style="width: 100%; padding: 15px;">
                  <div class="card-body">
                    <p><b>Reza Rahardian</b> create a new course</p>
                  </div>
                  <div class="card-footer text-center">
                    <span>27 September 2018 17.04</span>
                  </div>
                </div>
              </div>

              <div class="row mb-3">
                <div class="card" style="width: 100%; padding: 15px;">
                  <div class="card-body">
                    <p><b>Reza Rahardian</b> create a new course</p>
                  </div>
                  <div class="card-footer text-center">
                    <span>27 September 2018 17.04</span>
                  </div>
                </div>
              </div>

              <div class="row mb-3">
                <div class="card" style="width: 100%; padding: 15px;">
                  <div class="card-body">
                    <p><b>Reza Rahardian</b> create a new course</p>
                  </div>
                  <div class="card-footer text-center">
                    <span>27 September 2018 17.04</span>
                  </div>
                </div>
              </div>

              <div class="row mb-3">
                <div class="card" style="width: 100%; padding: 15px;">
                  <div class="card-body">
                    <p><b>Reza Rahardian</b> create a new course</p>
                  </div>
                  <div class="card-footer text-center">
                    <span>27 September 2018 17.04</span>
                  </div>
                </div>
              </div>

              <div class="row mb-3">
                <div class="card" style="width: 100%; padding: 15px;">
                  <div class="card-body">
                    <p><b>Reza Rahardian</b> create a new course</p>
                  </div>
                  <div class="card-footer text-center">
                    <span>27 September 2018 17.04</span>
                  </div>
                </div>
              </div>

              <div class="row mb-3">
                <div class="card" style="width: 100%; padding: 15px;">
                  <div class="card-body">
                    <p><b>Reza Rahardian</b> create a new course</p>
                  </div>
                  <div class="card-footer text-center">
                    <span>27 September 2018 17.04</span>
                  </div>
                </div>
              </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Full Height Modal Right -->
  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="../assets/js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="../assets/js/popper.min.js"></script>
  
  <script type="text/javascript" src="../assets/dist/fullcalendar/lib/moment.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
  <script src="../assets/js/addons/datatables.min.js"></script>
  <script src="../assets/dist/select2/js/select2.min.js"></script>
  
  <script src="../assets/dist/vjs/video.min.js"></script>
  
  <!-- Tambah ini  -->
  <script src="../assets/dist/vjs/Youtube.min.js"></script>

<script type="text/javascript" src="../assets/dist/qrcode/grid.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/version.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/detector.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/formatinf.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/errorlevel.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/bitmat.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/datablock.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/bmparser.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/datamask.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/rsdecoder.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/gf256poly.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/gf256.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/decoder.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/qrcode.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/findpat.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/alignpat.js"></script>
<script type="text/javascript" src="../assets/dist/qrcode/databr.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
  <script src="../assets/dist/fullcalendar/fullcalendar.js"></script>
  <script type="text/javascript" src="../assets/dist/wizard-master/prettify.js"></script>
  <script type="text/javascript" src="../assets/dist/wizard-master/jquery.bootstrap.wizard.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="../assets/js/mdb.min.js"></script>
<!--   <script type="text/javascript" src="../assets/js/mdb-pro.js"></script> -->
  <script type="text/javascript" src="../assets/js/script.js"></script>
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();

  </script>



</body>

</html>
