<?php

$super_user = $login = false;
if(isset($_SESSION['krywn_usernameSession'])) {

$login = true;
    if(cek_level($_SESSION['krywn_usernameSession']) == 0){
      $super_user = true;
   }  

}else{ //isset
    header('location: ../index.php');
}

$usid = $_SESSION['krywn_usernameSession'];
    if(isset($_SESSION['krywn_usernameSession'])){
      $article =tampilUsername($usid);
      
      while($row=mysqli_fetch_assoc($article)){
        $nama_depan_Id = $row['nama_depan'];
        $nama_belakang_Id = $row['nama_belakang'];
      }
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>CekRek - Machine Maintenance</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="../assets/css/addons/datatables.min.css" rel="stylesheet">
  <link href="../assets/dist/select2/css/select2.min.css" rel="stylesheet" >

  <!-- Material Design Bootstrap -->
  <link href="../assets/css/mdb.min.css" rel="stylesheet">
  <link href="../assets/css/mdb-pro.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../assets/dist/wizard-master/prettify.css">
  <link href="../assets/dist/vjs/video-js.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.css" rel="stylesheet">
  <link href="../assets/dist/fullcalendar/fullcalendar.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="../assets/css/style.css" rel="stylesheet">

  <link href="../assets/css/style.min.css" rel="stylesheet">
  <style>

.map-container{
overflow:hidden;
padding-bottom:56.25%;
position:relative;
height:0;
}
.map-container iframe{
left:0;
top:0;
height:100%;
width:100%;
position:absolute;
}
  </style>
</head>

<body class="grey lighten-3">

  <!--Main Navigation-->
  <header>

  <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar lanius-medium">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

      <li class="nav-item dropdown side-menu">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false"><strong>Menu</strong></a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="index.php">Dashboard</a>
          <a class="dropdown-item" href="pekerja-data.php">Master User</a>
          <a class="dropdown-item" href="mesin-data.php">Master Machine</a>
          <a class="dropdown-item" href="perintah-daftar.php">MO List</a>
          <a class="dropdown-item" href="perbaikan-daftar.php">Repair List</a>
          <a class="dropdown-item" href="perbaikan-daftar.php">Maintenance List</a>
        </div>
      </li>
         <!-- Dropdown -->
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false"><strong>Competency </strong></a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="course-mandatory.php">Mandatory Course</a>
          <a class="dropdown-item" href="competency-mandatory.php">Competency Mandatory</a>
        </div>
      </li> -->
      </ul>
      <ul class="navbar-nav nav-flex-icons">
        <li class="nav-item">
          <a class="nav-link">Halo, <?= $nama_depan_Id ?>!</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href=""data-toggle="modal" data-target="#notification"><i class="fa fa-bell"></i></a>
        </li>
           <!-- Dropdown -->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false"><i class="fa fa-user"></i></a>
          <div class="dropdown-menu dropdown-menu-right dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="profile.php"><i class="fa fa-user" style="margin-right: 8px;"></i> Profile</a>
            <a class="dropdown-item" href="logout.php"><i class="fas fa-sign-out-alt" style="margin-right: 8px;"></i> Logout</a>
          </div>
        </li>
      </ul>
    </div>
  </nav>

    <!-- Sidebar -->
    <div class="sidebar-fixed position-fixed" id="list-menu">

      <div class="logo">
        <a class="logo-wrapper waves-effect">
        <img src="../assets/img/user.png" class="img-fluid z-depth-1 rounded-circle"
      alt="Responsive image">
        </a>
        <h4><?= $nama_depan_Id ?> <?= $nama_belakang_Id ?></h4>
      </div>
      
      <div class="list-group list-group-flush">
        <a href="index.php" class="list-group-item list-group-item-action waves-effect">
          <div class="row">
            <div class="col-md-2">
              <i class="fas fa-home mr-3"></i>
            </div>
            <div class="col-md-10">Home
            </div>
          </div>
        </a>
        <a href="pekerja-data.php" class="list-group-item list-group-item-action waves-effect">
          <div class="row">
            <div class="col-md-2">  
              <i class="fas fa-users mr-3"></i>
            </div>
            <div class="col-md-10">Master User
            </div>
          </div>
        </a>
        <a href="mesin-data.php" class="list-group-item list-group-item-action waves-effect">
          <div class="row">
            <div class="col-md-2">  
              <i class="fas fa-cog mr-3"></i>
            </div>
            <div class="col-md-10">Master Machine
            </div>
          </div>
        </a>
        <a href="perintah-daftar.php" class="list-group-item list-group-item-action waves-effect">
          <div class="row">
            <div class="col-md-2">  
              <i class="fas fa-clipboard-list mr-3"></i>
            </div>
            <div class="col-md-10">MO List
            </div>
          </div>
        </a>
        <a href="perbaikan-daftar.php" class="list-group-item list-group-item-action waves-effect">
          <div class="row">
            <div class="col-md-2">  
              <i class="fas fa-clipboard-list mr-3"></i>
            </div>
            <div class="col-md-10">Repair List
            </div>
          </div>
        </a>
        <a href="perawatan-daftar.php" class="list-group-item list-group-item-action waves-effect">
          <div class="row">
            <div class="col-md-2">  
              <i class="fas fa-clipboard-list mr-3"></i>
            </div>
            <div class="col-md-10">Maintenance List
            </div>
          </div>
        </a>
      </div>

    </div>
    <!-- Sidebar -->

  </header>
  <!--Main Navigation-->